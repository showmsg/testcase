use strict;

#加载businessfunc/survey_sendMessage下的所有函数
my @filename = <businessfunc//survey_sendMessage//*.pl>;
require "$_" for(@filename);


sub survey_sendMessage_Single
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");

	#被测用户账号
	my @username = &readLine('1407996376_5_survey_0305.txt');
	#开始获取被测用户id
	my @user_id;
	for(@username){
		my $sql = "SELECT USER_ID FROM `USER_ACCOUNT` WHERE LOGIN_NAME='$_';";
		push @user_id, &operate_mysql(@survey,"$sql","select");
	}


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"yangxianming1");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#给单个用户发送消息
	########
	
	#testcase1 给单个用户发送消息;
	my $testcase = "sendMessageSingle";
	#发送消息
	my $ms_tital = "yxmtital".time();	#消息标题,用时间戳来标记唯一
	my $ms_info = "yxminfo".time();	#消息内容
	print "ms_tital:$ms_tital\n";
	&sendmessage($mgtbrowser,$mgt_addr,$ms_info,2,-1,$ms_tital,$user_id[0]);
	

	#收消息并验证消息
	#手机端的登录请求
	my ($mobrowser,$accessToken);
	print "$username[0],start to test--\n";
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0],'MTExMTExMTE=');
	my @verif;	#验证数组,全1才算通过
	push @verif,&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase);
	
	#手机登录请求
	#获得token
	print "$username[1],start to test--\n";
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[1],'MTExMTExMTE=');
	#收消息并验证消息
	if(&receive_msg($mobrowser,$accessToken,$mob_addr,$ms_tital,$ms_info,$testcase)){
		print "给单个用户发送消息,无关的用户收到了消息,测试失败\n";
		push @verif,0;
	}else{
		print "给单个用户发送消息,无关的用户没有收到了消息,测试通过\n";
		push @verif,1;
	}

	#循环检查结果列表,并返回测试结果
	for(@verif){
		if($_ == 0){
			print "给单个用户发送消息,部分验证错误\n";
			return 0 ;
		}
	}
	print "给单个用户发送消息,测试全部验证正确\n";
	return 1;

}
1;
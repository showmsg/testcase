#注册用户
#每个&mob_reg函数只可以注册一批相同属性的用户
#可以多个mob_reg函数


use strict;

#加载aa/bb下的所有函数
#my @filename = <aa//bb//*.pl>;
#require "$_" for(@filename);

sub survey_regist
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	

	#######
	#手机端注册
	#######
	#因为不是登录所有无法获取网络访问的对象,这里创建一个http的对象
	my $ck=HTTP::Cookies->new();
	my $browser=LWP::UserAgent->new(agent=>'Mozilla/5.0',cookie_jar=>$ck);

	my %updateInfo;	#初始化用户资料
	my $totalcount = 0;
	mkdir("account") unless(-d "account");	#查找目录账户目录是否存在

	my @threadHead;
	
	
	
	#注册人数
	my $reg_count = 100;
	#并发数
	my $bingfa = 5;
	%updateInfo = (		#值定义 a-b随机选择a到b包括ab的随机数  a,b,c列表值选择abc中的任意一个,(生日和性别不适用)
		'birthday' => '',
		'sex' => '1-2',		#1男 2女 不限男女不用赋值
		'education' => '1-6',		#1-6	学历_
		'income' => '1-11',				#1-11	月收入_
		'incomeFamily' => '1-11',	#1-11	家庭月收入_
		'marriage' => '9-12',		#9-12	婚姻_
		'position' => '1-9',		#1-9	职位_
		'profession' => '1-11',	#1-11	职业_
		'vocation' => '1-18',			#1-18	行业_
	);
	#push @reg_count,&mob_reg($browser,\@survey,$mob_addr,$reg_count,\%updateInfo);
	
	#根据账号总数来生成账号列表
	my @accountList;
	
	for(1..$reg_count){
		my @mobFirsthree = qw{134 135 136 137 138 139 150 151 152 157 158 159 130 131 132 155 156};
		my $account_Prefix_2 = sprintf("%04d", (substr(time(),5,4)));
		my $account_Prefix = $mobFirsthree[&random(0,$#mobFirsthree)] . $account_Prefix_2;
		my $len = sprintf("%04d",$_);
		push @accountList,$account_Prefix.$len;
	}
	
	#开始做用户循环，到达并发量就执行并发操作 ->join()
	#开始执行多线程
	my $doBinfa = 0;
	my @threadHead;
	my @threadReturn;
	for my $oneacc (@accountList){
		push @threadHead ,threads->create(
			sub{
				&mob_reg($browser,\@survey,$mob_addr,$oneacc,\%updateInfo);
			}
		);
		$doBinfa++;
		if($doBinfa eq $bingfa){
			for(@threadHead){
				push @threadReturn,$_->join();
			}
			$doBinfa = 0;
			@threadHead = ();
			print "--do join--\n";
		}
	}
	
	
		
	#将创建的账户存入文件
	my $filename = time()."_".$reg_count."_".$survey[0].".txt";
	open ACC,">>account/$filename";
	print ACC "$_\n" for(@accountList);
	close ACC;
	print "账户文件创建完毕,文件名 $filename \n";

	return -1;
}
1;
use strict;



sub survey_physicalReward
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#初始化一些信息和资源
	#数据库链接信息
	my @survey = ("survey_0305","10.201.2.13","3306","mq-user","redhat");
	my $sql = "SELECT USER_ID FROM `USER_ACCOUNT` WHERE LOGIN_NAME = \"13564324375\"";
	#my $a = &operate_mysql(@survey,"$sql","select");
	#管理后台地址
	#my $mgt_addr = "http://10.201.2.12";
	my $mgt_addr = "http://10.201.70.81";
	my @user_id = ("10301","10101","10293");
	#手机服务地址
	#my $mob_addr = "http://10.201.2.12:8080";
	my $mob_addr = "http://10.201.70.82:8080";
	my @username = ("15021809097","15900826057","15715559622");


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"jiangw");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#创建问卷
	########
	#初始化数据
	my $awardItemName = "one kiss".time();
	my %parameter_group = (
		paperImage=>"",
		awardItemImage=>"",
		myfile=>[""],
		projectCode=>"perltest",
		surveyTitle=>time()."_perlscript",
		surveyDesc=>"perlscript create survey",
		flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
		hasCargo=>"0",				#物流(1.需要  0.不需要)
		presentName=>"",
		presentAmount=>"",
		presentCount=>"",
		awardTypeStr=>"2",			#1.现金  2.实物
		awardItemName=>$awardItemName,
		awardItemPrice=>"12",
		awardItemNum=>"100",
		awardItemDesc=>"one kiss,forever love",
		awardImagefile=>["resources//onekiss.jpg"],
		surveyHelp=>"",
		flagBooking=>"0",			#是否是未来任务(1.是  0.否)
		flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
		flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
	
	
	
	##############################
	#创建edit_question
	#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
	#数字填空，文字填空，需要清空数组@Options
	#视频题@Options格式：("视频文件的本地地址")
	#将数组传入createquest函数
	#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
	#############################
	
	#申明并初始化部分数据
	#@options (选项名字,选项图片,..) 的规则排列
	#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
	my (@Options,$type,$sort,$flagReference,@questid,$temp);
	$sort = 1;				#排序从1开始
	$flagReference = 0;		#默认都不是引用题
	@questid;		#引用题的引用源
	
	#初始化数据(单选)
	@Options = ("danxuanxuanxiang11","",
					"danxuanxuanxiang12","",
					"danxuanxuanxiang13","",
					"danxuanxuanxiang14","",
					"danxuanxuanxiang15","");
	$type = 1;	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question($mgtbrowser,$mgt_addr,$type,$surveyid,$sort++,"quet_radio","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	
	##########
	#创建逻辑
	##########
	#($typename,$surveyid,$fromquest,$selected,$toquest)
	#目前只支持单个逻辑与条件,单题跳转单选项跳转,termination,jump


	
	########
	#创建配额
	########
	#1女  0男
	my %quota;
	#提交一个
	%quota = (
		'age'=>"",
		'amount'=>"",
		'area'=>"",
		'surveyCount'=>11,
		'surveyId'=>$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,\%quota);
	
	
	####
	#提交问卷
	####survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
	####
	#审核上家
	####
	my %auditconfig = ();
	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
	
	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","4");
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");
	sleep 1;
	
	
	
	
	
	
	
	
	#######
	#手机端验证是否存在该问卷
	#######

	#手机端的登录请求
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0]);
	
	
	my $return;
	for(1..30){		#30秒超时设置 每秒发送请求并验证结果
		$return = $mobrowser->get("$mob_addr/survey-ws/surveysvc/homepageList?sortType=2&pagesize=10&type=1&page=0&surveyid=-1",
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",)->content;
		&verification("mob","survey_exist.surveys",$mobrowser,$return);
		while($return =~ /"awardName":"(.*?)"/gi){
			if($awardItemName eq $1){
				print "find it -- \"awardName\":\"$1\"\n";
				return 1;
			}
		}
		sleep 1;
	}
	print LOG "exist - $return\n";
	return 0;
	
}
1;
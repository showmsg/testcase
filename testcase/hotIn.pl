#点击购买积分商城中的商品


use strict;

sub hotIn
{
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	#开放平台地址
	my $open_addr = &config("","open_addr");
	#wap地址
	my $wap_addr = &config("","wap_addr");
	
	#并发量
	my $bingfa = 10;			
	#获取测试数据	$data{'字段名字'}->[第几行数据的下标]  $data{'userAccount'}->[n] 
	my %data = &readData('1411751731_10_survey_0305.txt',',');
	
	
	#单次循环并发
	my $doBinfa = 0;
	my @threadHead;
	my ($wapBrowser, $doExchangeReturn, $mobrowser, $accessToken);
	my $count = 0;
	for my $index (0..$data{'maxDataLine'}){
		my $useraccount = $data{'account'}->[$index];
		my $activityId = $data{'activityId'}->[$index];
		#计数器
		$count++;
		print "NO $count $useraccount \n";
		#创建抽奖线程
		push @threadHead ,threads->create(
			sub{
				#手机登录
				($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
				next unless($mobrowser);
				print "Token:($accessToken) ";
				#逐个登录wap
				$wapBrowser = &wapLogin($wap_addr,$accessToken);
				print "wapBrowser:($wapBrowser) \n";
				#执行商城兑换
				$doExchangeReturn = &doExchange($wap_addr, \@survey, $wapBrowser, $activityId);
				myprint("encode", "doExchangeReturn:($doExchangeReturn) \n");
			}
		);
		$doBinfa++;
		
		if($doBinfa eq $bingfa){
			for(@threadHead){
				$_->join();
			}
			$doBinfa = 0;
			@threadHead = ();
			print "--do join--\n";
		}
	}
	
	



	return -1;

}
1;
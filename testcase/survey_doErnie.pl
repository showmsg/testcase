#单独执行用户摇奖功能
use strict;

sub survey_doErnie
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件
	my %data = &readExcel('1411981126_10_survey_0305.xls','Sheet1');
	
	#被测试的问卷id从文件中读取
	my @resid = &readLine("data//surveyid.txt");
	my $surveyid = $resid[0];

	#普通单线程模式
	#加载已经创建的用户列表
	#统计分享额
	my $count = 0;
	my ($mobrowser, $accessToken, $sunnyId, $amount, $currentAmount);
	
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		my $useraccount = $data{'account'}->[$index];	#用户账号
		my $sharedContent = $data{'sharedContent'}->[$index] . time() . &random(1,100);	#用户账号
		print "被测问卷id:$surveyid\n";
		
		#######
		#手机端模拟用户登录并返回token
		#######
		print "useraccount:$useraccount , ";
		($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
		#print "accessToken:$accessToken ,";
		
		#获取sunnyid
		$sunnyId = &getSurveySunnyList($mob_addr, \@survey, $mobrowser, $accessToken, $surveyid);
		print "(sunnyId:$sunnyId) ";
		
		#获取用户资料
		$currentAmount = &userCenter($mob_addr, \@survey, $mobrowser, $accessToken, 'currentAmount');
		print "(currentAmount:$currentAmount) ";
		
		#用户抽奖
		$amount = &doErnie($mob_addr, \@survey, $mobrowser, $accessToken, $sunnyId);
		print "(doErnie:$amount) ";
		
		#获取用户资料
		$currentAmount = &userCenter($mob_addr, \@survey, $mobrowser, $accessToken, 'currentAmount');
		print "(currentAmount:$currentAmount) ";
		
		#开始分享-有的问卷只有分享后才能获奖
		&shareGrantPrize($mob_addr, \@survey, $mobrowser, $accessToken, $sunnyId, $sharedContent);
		print "(sharedContent:$sharedContent) ";
		
		#获取用户资料
		$currentAmount = &userCenter($mob_addr, \@survey, $mobrowser, $accessToken, 'currentAmount');
		print "(currentAmount:$currentAmount) ";
		
		#统计抽奖总额
		$count = $count + $amount;
		print "\n\n";
	}
	print "总计抽取额 : $count\n";


	return -1;
}
1;
#精彩回顾
use strict;

sub survey_review
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件
	my %data = &readExcel('forSurveyReview\\1411981126_10_survey_0305.xls','Sheet1');
	
	#普通单线程模式
	#加载已经创建的用户列表
	#统计分享额
	my $count = 0;
	my ($mobrowser, $accessToken, $sunnyId, $amount, $currScore);
	
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		my $account = $data{'account'}->[$index];	#用户账号
		my $contentId = $data{'contentId'}->[$index];	#内容id
		my $type = $data{'type'}->[$index];	#赞类型
		my $content = $data{'content'}->[$index];	#赞类型
	
		#######
		#手机端模拟用户登录并返回token
		#######
		print "account:$account , ";
		($mobrowser,$accessToken) = &moblogin($mob_addr,$account,'MTExMTExMTE=');
		#print "accessToken:$accessToken ,";
		
		#获取用户资料
		$currScore = &userCenter($mob_addr, \@survey, $mobrowser, $accessToken, 'currScore');
		print "(currScore:$currScore) ";
	
		#给精彩回顾点个赞
	#	my @r = &vote($mob_addr, \@survey, $mobrowser, $accessToken, $contentId, $type);
	#	print $r[0].", ". $r[1] ."\n";
		
		#发个更贴,不回复某人
		#参postReply数分别是(contentId, contentType, rootId, commentId, content, imgUrls)
		my @postReply = ($contentId, '1', '', '', $content, '');
		my $rewardPoints = &reply($mob_addr, \@survey, $mobrowser, $accessToken, \@postReply);
		print "rewardPoints:$rewardPoints ";
		
		#获取用户资料
		$currScore = &userCenter($mob_addr, \@survey, $mobrowser, $accessToken, 'currScore');
		print "(currScore:$currScore) ";
		
		print "\n";
	}


	return -1;
}
1;
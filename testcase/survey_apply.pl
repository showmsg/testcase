#用户预报名

use strict;

#加载aa/bb下的所有函数
#my @filename = <aa//bb//*.pl>;
#require "$_" for(@filename);

sub survey_apply
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
	my @acc_file = ("1408089991_200_survey_0305.txt");
	#问卷id
	my $surveyid = "12974";
	
	#加载已经创建的用户列表
	for my $accfile (@acc_file){
		if(open READ,"account//$accfile"){
			print "account目录下 $accfile 文件成功打开\n开始读取\n";
		}else{
			die "account目录下无法打开 $accfile 文件\n";
		}
		my @userlist = <READ>;
		print "被测问卷id:$surveyid\n";
		
		#######
		#手机端模拟用户答题的请求
		#######
		&userApply($mob_addr,\@survey,\@userlist,$surveyid,1);
	}
	
	return -1;
}
1;
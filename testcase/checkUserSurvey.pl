﻿use strict;

#验证用户是否可以看到问卷

use strict;

sub checkUserSurvey
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件
	my %data = &readExcel('1411981127_520_survey_0305.xls','Sheet1');
	
	#被测试的问卷id
	my $surveyid = "13774";
	
	#加载已经创建的用户列表
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		my $useraccount = $data{'account'}->[$index];	#用户账号
		&myprint("encode", "被测问卷id:$surveyid\n");
		
		######
		#手机端验证是否存在该问卷
		#######
		&checkMySurvey($mob_addr,$surveyid,$useraccount);
	}
	return -1;
}
1;


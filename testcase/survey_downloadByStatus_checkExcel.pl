#描述:调查列表->审核答卷->点击到处excel


use strict;

sub survey_downloadByStatus_checkExcel
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");

	my @user_id = ("10301","10101","10293");
	my @username = ("15021809097","15900826057","15715559622");
	#测试结果值
	my $result = 1;


	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = &mgtlogin($mgt_addr,"jiangw");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	

	####
	#下载问卷统计报告
	####survey/a/submitSurvey?surveyId=3150,3625
	my $surveyid = "4965"; #5273 4965 4947
	my $filename;
	$filename = &download_file($mgtbrowser , "$mgt_addr/check/downloadByStatus/$surveyid/-1/0/-1" , 2);
	
	#####
	#分析报告
	#读取下载的excel
	####
	
	my $parser   = Spreadsheet::ParseExcel->new();
	#$filename = encode("gbk", decode("utf-8", $filename));   全部_厨房电器品牌知名度调研14051510.xls
	my $workbook = $parser->parse($filename);
	#my $workbook = $parser->parse('全部_厨房电器品牌知名度调研14051510.xls');
	
	if ( !defined $workbook ) {
		die $parser->error(), ".\n";
	}
	
	my %userhash = ();
	for my $worksheet ( $workbook->worksheets() ) {
		my $worksheetString = $worksheet->get_name();
		#$worksheetString = &myprint("encode", "$worksheetString");
		$worksheetString = encode("gbk", $worksheetString);
		next unless($worksheetString eq "通过");
		print "worksheet = $worksheetString\n";
		
		my ( $row_min, $row_max ) = $worksheet->row_range();	#row 列
		my ( $col_min, $col_max ) = $worksheet->col_range();	#col 行
	
		#print " $row_min, $row_max \n";
		#print " $col_min, $col_max \n";
		
		#寻找row=0的Q开头位置
		my ($fromQ, $toQ);	#这里获取的是col
		my $i = 0;
		for my $col ( $col_min .. $col_max ){
			my $cell = $worksheet->get_cell( 0, $col );
			next unless $cell;
			if ($cell->value() =~ /q/i){
				unless($i){
					$fromQ = $col;
					$i++;
				}
				$toQ = $col;
			}
		}
		
		
		for my $row ( $row_min .. $row_max ) {
			my $cell = $worksheet->get_cell( $row, 1 );
			next unless $cell;
			my $answerId = $cell->value();
			#print "id:$answerId\n";
			my @answerArr = ();
			if ($answerId =~ /\d+/){
				for my $answer ($fromQ .. $toQ) {
					my $aa = $worksheet->get_cell( 0, $answer );
					my $bb = $worksheet->get_cell( $row, $answer );
					#print "aabb:    $bb  --\n";
					next unless ($aa || $bb);
					my $quest_titil = $aa->value();
					my $user_answer;
					unless($bb){
						$user_answer = "";
					}else{
						$user_answer = $bb->value();
					}
					#print "user_answer : $user_answer\n";
					push @answerArr,($quest_titil.",".$user_answer);
					
				}
				$userhash{$answerId} = join ";",@answerArr;
			}
		}
	}
	
#	open FF,">excelog.txt";
#	while(my($k,$v) = each %userhash){
#		$k = encode("utf-8",  $k);
#		$v = encode("utf-8",  $v);
#		print FF "$k : $v\n";
#	}
	
	####
	#分析报告
	#随机找出抽样样本
	####
	my $Sample = 10;
	my @userarr = keys %userhash;
	int (0+rand $#userarr);
	my %save_sample;
	while(1){
		my $randkey = $userarr[int (0+rand $#userarr)];
		$save_sample{$randkey} = $userhash{$randkey};
		last if(keys %save_sample eq $Sample);
		
	}
	
	open FF,">excelog.txt";
	while(my($k,$v) = each %save_sample){
		$k = encode("utf-8",  $k);
		$v = encode("utf-8",  $v);
		print FF "$k : $v\n";
	}
	
	####
	#分析报告
	#比对数据库信息
	####
	my $json = JSON->new->utf8;
	my $json_obj;
	
	#这段sql返回本问卷的 题号
	my @questionlist = &operate_mysql(@survey,"SELECT ID,SORTED_NUM FROM SURVEY_QUESTION WHERE SURVEY_ID = $surveyid","select");
	for my $userSurveyId (keys %save_sample){	#excel抽样的样本，
		print "$userSurveyId-checking : ";
		my $sql = "SELECT ANSWERS FROM `USER_SURVEY` WHERE ID = $userSurveyId;";
		my $returnjson = &operate_mysql(@survey,"$sql","select");	#这里返回单个用户的答题信息
		#对返回的数据做处理前后加上 {"answer":  ..  } 以便可以哦通过json模块来处理数据
		$returnjson = '{"answer":' . $returnjson . '}';
		
		#print "$returnjson\n";
		$json_obj = $json->decode("$returnjson");
		
		my $c = 1;
		my ($sqlAnswer,$excelAnswer);
		for my $value (@questionlist){		#用 SORTED_NUM 作为比对的引号 在c==2时开始做对比校验
			#print "debug--value $value\n";
			
			if($c eq 1){	#从sql返回值中抓取答案
				for my $key (@{$json_obj->{'answer'}}){
					#print "key : $key\n";
					#print "qid--" . $key->{'qid'} . "\n";
					if ($key->{'qid'} eq $value){
						if(ref($key->{'value'}->{'input'}) eq "ARRAY"){
							#print "debug--input(arr) " . @{$key->{'value'}->{'input'}} . "\n";
							$sqlAnswer = join "", @{$key->{'value'}->{'input'}};
						}else{
							#print "debug--input(noarr) " . $key->{'value'}->{'input'} . "\n"
							$sqlAnswer = $key->{'value'}->{'input'};
						}
						last;
						#print "1111111\n";
					}
				}
				
				#print "11111111111111\n";
				$c++;
				next;
			}
			if($c eq 2){	#抓取excel样本中的答案
				my $qList1 = $save_sample{$userSurveyId};
				my @qList2 = split /;/,$qList1;
				my %qlist_hash = ();
				for (@qList2){
					my ($quest_index, $answer) = split /,/,$_;
					#print "debug--quest_index,answer   $quest_index, $answer \n";
					my ($quest_forowd, $quest_short) = split /-/,$quest_index;
					#print "debug--quest_forowd,quest_short   $quest_forowd, $quest_short \n";
					$qlist_hash{$quest_forowd} = $qlist_hash{$quest_forowd} . $answer; 
				}
				#while(my($k,$v) = each %qlist_hash){
				#	print "$k,$v\n";
				#}
				#print "2222222\n";
				
				#开始比对excel和sql返回的数据是否一致
				my $Qvalue = "Q" . $value;
				print $qlist_hash{$Qvalue} ."-". $sqlAnswer;
				if($qlist_hash{$Qvalue} eq $sqlAnswer){
					print " ";
				}else{
					print "(ERROR) ";
					$result = 0;
				}

				#清空结果值
				$sqlAnswer = 0;
				$excelAnswer = 0;
				$c = 1;
				next;
			}
		}
		print "\n";

	}

	return $result;
}
1;
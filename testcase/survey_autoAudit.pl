﻿use strict;


sub survey_autoAudit
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	my @username = ("15021809097","15900826057","15715559622");
	
	#三端代码负责这条测试用例的不同功能，用一个数组来控制不同的模块是否运行
	#创建问卷、用户作答、验证答卷
	my @control = (1,1,0);
	
	my $surveyid = "";
	#################
	#开始创建测试问卷
	#################
	if($control[0]){
		myprint("encode", "开始创建测试问卷\n");
		
		#配额
		my $surveyCount_1 = "100"; #预设值,如果没有赋值需要用户输入
		if($surveyCount_1 eq ""){
			&myprint("encode", "问卷的配额总量? ");
			chomp(my $surveyCount_1 = <STDIN>);
		}
		&myprint("encode", "开始创建问卷,配额:$surveyCount_1\n");
	
		#管理后台登录请求
		my ($mgtbrowser);
		($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");
		#($mgtbrowser) = mgtloginForHttps($mgt_https,"yangxianming1");
		########
		#测试代码(这里开始编写主要测试逻辑业务)
		########
		
		########
		#创建问卷
		########
		#初始化数据
		my %parameter_group = (
			paperImage=>"",
			awardItemImage=>"",
			myfile=>"",
			projectCode=>"perltest",
			surveyTitle=>time()."_perlscript_描述:审核答卷-其中包括驳回、通过、批量通过、批量驳回",
			surveyDesc=>"perlscript create survey--描述:审核答卷-其中包括驳回、通过、批量通过、批量驳回",
			flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
			hasCargo=>"0",				#物流(1.需要  0.不需要)
			lotteryTypeStr=>'0',
			quotaProportion=>'',
			participationProportion=>'',
			presentName=>"",
			presentAmount=>"",
			presentCount=>"",
			awardTypeStr=>"1",			#1.现金 2.非现金 3.红包
			awardAmount=>"1",
			averageAward=>"0",
			sigelHighAward=>"0",
			sigelLowAward=>"0",
			awardItemName=>"",
			awardItemPrice=>"",
			awardItemNum=>"",
			awardItemDesc=>"",
			awardImagefile=>"",
			surveyHelp=>"",
			flagBooking=>"0",			#是否是未来任务(1.是  0.否)
			flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
			flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
		);
		#执行创建问卷函数,并返回surveyid
		$surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
		
		
		
		##############################
		#创建edit_question
		#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
		#数字填空，文字填空，需要清空数组@Options
		#视频题@Options格式：("视频文件的本地地址")
		#将数组传入createquest函数
		#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
		#############################
		
		#申明并初始化部分数据
		#@options (选项名字,选项图片,..) 的规则排列
		#type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频)
		my (@Options,$type,$sort,$flagReference,@questid,$temp);
		$sort = 1;				#排序从1开始
		$flagReference = 0;		#默认都不是引用题
		@questid;		#引用题的引用源
		
		#初始化数据(单选) 1
		@Options = ("选项1","",
					"选项2","",
					"选项3","",
					"选项4","",
					"选项5","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question(	$mgtbrowser,$mgt_addr,
								$type,$surveyid,$sort++,
								"单选题","",\@Options,
								$flagReference,\@questid,""
		);
		push @questid,$temp;
		
		#初始化数据(单选) 2
		@Options = ("选项1","",
					"选项2","",
					"选项3","",
					"选项4","",
					"选项5","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question(	$mgtbrowser,$mgt_addr,
								$type,$surveyid,$sort++,
								"单选题","",\@Options,
								$flagReference,\@questid,""
		);
		push @questid,$temp;
		
		#初始化数据(单选) 3
		@Options = ("选项1","",
					"选项2","",
					"选项3","",
					"选项4","",
					"选项5","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question(	$mgtbrowser,$mgt_addr,
								$type,$surveyid,$sort++,
								"单选题","",\@Options,
								$flagReference,\@questid,""
		);
		push @questid,$temp;
		
		#初始化数据(单选) 4
		@Options = ("选项1","",
					"选项2","",
					"选项3","",
					"选项4","",
					"选项5","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question(	$mgtbrowser,$mgt_addr,
								$type,$surveyid,$sort++,
								"单选题","",\@Options,
								$flagReference,\@questid,""
		);
		push @questid,$temp;
		
		#初始化数据(单选) 5
		@Options = ("选项1","",
					"选项2","",
					"选项3","",
					"选项4","",
					"选项5","");
		$type = 1;	
		#执行创建题目函数(一次创建一个题目)
		$temp = &edit_question(	$mgtbrowser,$mgt_addr,
								$type,$surveyid,$sort++,
								"单选题","",\@Options,
								$flagReference,\@questid,""
		);
		push @questid,$temp;
	
		#创建逻辑
		my %logichash;
		%logichash = (
			'surveyId'=>$surveyid,
		);
		#一个逻辑终止
		$logichash{'logicList[0].conditions[0].qid'}=$questid[0];	#题目的qid
		$logichash{'logicList[0].conditions[0].optIds'}=2;			#题目选项
		$logichash{'logicList[0].conditions[0].selected'}=1;		#1.选择  0.不选择的
		$logichash{'logicList[0].typeName'}='termination';
		#一个逻辑
		$logichash{'logicList[1].conditions[0].qid'}=$questid[1];
		$logichash{'logicList[1].conditions[0].optIds'}=2;
		$logichash{'logicList[1].conditions[0].selected'}=1;
		$logichash{'logicList[1].result.destQid'}=$questid[$#questid];
		$logichash{'logicList[1].typeName'}='jump';
		#调试
		#while(my($k,$v) = each %logichash){
		#	print "debug---$k,$v\n";
		#}
#		&editlogic($mgtbrowser,$mgt_addr,\%logichash);
		
		#创建配额
		#1女  0男
		my (@quota_1,@quota_2);
		#差异属性的配合设置
		@quota_1 = (
			'age',"",
			'amount',"",
			'area',"",
			'reviewer','213',
			'reviewer','213',
			'sex',"",
			'surveyCount',$surveyCount_1,
			'surveyId',$surveyid,
			);
		#参与条件的配合设置
		@quota_2 = (
			'quotaType','1',
			'surveyId',$surveyid,
			'totalSamples','',
			);
		&addsurveyquota($mgtbrowser,$mgt_addr,$surveyid,\@quota_1,\@quota_2);
		
	
		#提交问卷
		&submit($mgtbrowser,$mgt_addr,$surveyid);
		
		#审核上家
		my %auditconfig = (
			"awardAmount" => 2,
			"flagBooking" => "",
			"terminations[0].awardAmount" => "0.1",	#终止奖励金额
			"terminations[0].awardNum" => "50",		#终止奖励人数
			"terminations[0].questionId" => "$questid[0]",	#终止的题号
			"terminations[0].optionId" => "2",		#选项号
			"terminations[0].selected" => "1",		#1.选择
			"terminations[0].sendAwardNum" => "0",	
			"terminations[0].sortedNum" => "1",		#问题序号
		);
		&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
		
		#立刻上架操作
		my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","10");
		my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
		&operate_mysql(@survey,"$sql","update");
		sleep 1;
	
		#手机端验证是否存在该问卷
		&checkMySurvey($mob_addr,$surveyid,$username[0]);
		
	}
	
	
	#################
	#开始虚拟用户答题
	#场景设置为10人超时，10人逻辑终止，80人提交
	#################
	
	
	my @returnUser;#接收用户答题信息
	if($control[1]){
		&myprint("encode", "开始虚拟用户答题\n");
		
		#需要读取的账号文件(mob_reg这个测试用例可以创建用户群)
		my $acc_file = '1355834_10_survey_0305.txt';
		if($acc_file eq ""){
			&myprint("encode", "输入account目录下的一个用户列表文件名:");
			chomp(my $acc_file = <STDIN>);
		}

		#只有在单独执"虚拟用户答题"代码块时需要用户来输入
		if($surveyid eq ""){
			&myprint("encode", "输入用户需要作答的问卷id");
			chomp($surveyid = <STDIN>);
		}
		
		#加载已经创建的用户列表
		if(open READ,"account//$acc_file"){
			&myprint("encode", "account目录下 $acc_file 文件成功打开\n开始读取\n");
		}else{
			die "Can not open file -> $acc_file \n";
		}
		my @userlist = <READ>;
		&myprint("encode", "被测问卷id:$surveyid\n");
		
		#手机端模拟用户答题的请求 
		#mode1普通随机答题
		#mode2(设置有超时用户和被终结用户)
		my $mode = 1;
		@returnUser = &userAnswer($mob_addr,\@survey,\@userlist,$surveyid,$mode);
	}
	

	
	
	#################
	#验证
	#################
	if($control[2]){
		#print "$_\n" for(@returnUser);
		#现对答卷数据做预处理标记 超时timeout 终结terminator 普通用户general 跳题jump
		#目前先通过答题数量来判断不同类型的用户
		my (@timeoutUser,@terminatorUser,@generalUser,@jumpUser);
		for(@returnUser){
			my @sp = split /;/,$_;
			push (@timeoutUser,$_) if($#sp == 0);
			push (@generalUser,$_) if($#sp == 5);
			push (@terminatorUser,$_) if($#sp == 1);
			push (@jumpUser,$_) if($#sp>1 && $#sp<5);
		}
		#print "$_\n" for(@generalUser);

		#管理后台登录请求
		my ($mgtbrowser);
		($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");
		#控制5个验证点是否执行
		my @verify = (0,0,0,0,0);
		if($verify[0]){
			#验证审核列表中是否有 对应数量的超时用户 终结用户 和普通答题用户
			#获取超时的用户
			#http://10.201.70.81/check/survey_answer_list/1/4920/5  超时
			my $answer_list_url = "/check/survey_answer_list/";
			my $answerCode = "5";	#$answerCode 5超时  2提交   6逻辑终止
			&survey_answer_list($mgtbrowser,$mgt_addr,$answer_list_url,$surveyid,$answerCode);

			#http://10.201.70.81/check/survey_answer_list/1/4920/2  提交
			
			#http://10.201.70.81/check/survey_answer_list/1/4920/6  逻辑终止
			
			
		}
		if($verify[1]){
			#验证查看按钮中 所有用户答题信息是否正确
			
		}
		if($verify[2] && @generalUser>0){
			#验证批量审核通过，以及单个审核通过，并能收到消息
			#强制判断打完全部5提的才是可以审核的普通用户而非终结用户，如果是有跳题的测试就需要再做判断
			#http://10.201.70.81/check/batchCheckOk/27825574,27825575
			#先获取答卷id
			my ($loginNameStr,@temp);
			for(@generalUser){
				push @temp,(split /;/,$_)[0];
			}
			$loginNameStr = join ",",@temp;
			#print "$loginNameStr\n";
			my $sql = "SELECT ID FROM USER_SURVEY WHERE USER_ID IN (SELECT USER_ID FROM USER_ACCOUNT WHERE LOGIN_NAME IN ($loginNameStr) AND SURVEY_ID = \'$surveyid\')";
			my @id = &operate_mysql(@survey,"$sql","select");
			my $idString = join ",",@id;
			
			my $return = $mgtbrowser->get("$mgt_addr/check/batchCheckOk/$idString")->content;
			&verification("mgt","batchCheckOk",$mgtbrowser,$return);
			myprint("encode","$return\n");
		}
		if($verify[3]){
			#验证批量驳回，以及单个驳回，并能收到消息
			
		}
		if($verify[4]){
			#点击终止奖励按钮，列表中显示用户终止逻辑的奖励以获得，并能收到消息
			
		}
	
	}
	
}
1;
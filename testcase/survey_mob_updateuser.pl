#多线程的快速提交测试

use strict;



sub survey_mob_updateuser
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#初始化一些信息和资源
	#数据库链接信息
	my @survey = ("survey_0305","10.201.2.13","3306","mq-user","redhat");
	#my $sql = "SELECT count(*) FROM USER_PROFILE_MODIFY WHERE USER_ID=10293";
	#my $a = &operate_mysql(@survey,"$sql","select");

	#手机服务地址
	#my $mob_addr = "http://10.201.2.12:8080";
	my $mob_addr = "http://10.201.70.82:8080";
	my @username = ("13564324373","15900826057","15715559622");
	my @user_id = ("206853","10101","10293");



	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########

	
	
	#######
	#手机端验证是否存在该问卷
	#######

	#手机端的登录请求
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = &moblogin($mob_addr,$username[0]);
	
	####
	#开始验证
	####
	my $numthreads = 10;
	my @threadhandles = ();
	my @results = ();

	#&post_updateuser($mobrowser,$mob_addr);
	
	for(my $i=0 ; $i<$numthreads ; $i++){
		my $thread = threads->create(\&post_updateuser($mobrowser,$mob_addr,$accessToken));
		print "__debug__\n";
		push(@threadhandles, $thread);
	}
	
	while($#threadhandles>0){
		my $handle = pop(@threadhandles);
		my $result = $handle->join();
		push(@results,$result);
		print "result:$result\n";
	}
	
	my $sql = "SELECT count(*) FROM USER_PROFILE_MODIFY WHERE USER_ID=206853";
	my $a = &operate_mysql(@survey,"$sql","select");
	print "count: $a\n";
	
	#重复测试的删除sql
	#DELETE from USER_PROFILE_MODIFY WHERE USER_ID=206853
	
	
	
	sub post_updateuser
	{
		my ($mobrowser,$mob_addr,$accessToken) = @_;
		my $return = $mobrowser->post("$mob_addr/survey-ws/usersvc/updateuser",
		{
			education=>'1',
			income=>'1',
			marriage=>'1',
			position=>'1',
			profession=>'1',
			vocation=>'1',
			incomeFamily=>'1',
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",)->content;
		
		&verification("mob","survey_exist.surveys",$mobrowser,$return);
		#while($return =~ /"surveyId":(.*?),/gi){
		#	#print "--debug-- $1\n";
		#	if($surveyid eq $1){
		#		print "find it -- surveyId:$1\n";
		#		return 1;
		#	}
		#}

		#print "Can not find surveyId -- $surveyid\n";
		#print LOG "exist - $return\n";
		return 1;
	
	}
	
}
1;
#点击抽奖

use strict;

sub wap_executeLottery
{
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	#开放平台地址
	my $open_addr = &config("","open_addr");
	#wap地址
	my $wap_addr = &config("","wap_addr");

	#被测试的问卷id
	my $bingfa = 5;			#并发量
	
	#逐个登录mob并且获取token
	my @userListArr;
	my @userlist;
	my @tokenList;
	my ($mobrowser,$accessToken);
	
	my %data = &readData('1413188498_2500_survey_0305.txt',',');
	
	#多次循环
	#手机登录
#	for my $useraccount (@userlist){
#		next if($useraccount =~ /^#/);
#		chomp($useraccount);
#		($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
#		next unless($mobrowser);
#		push @tokenList,$accessToken;
#	}
#
#	#逐个登录wap
#	my @wapBrowserList;
#	for my $token (@tokenList){
#		push @wapBrowserList, &wapLogin($wap_addr,$token);
#	}
#	
#	#执行抽奖接口
#	print "total--- $#wapBrowserList\n";
#	for(1..2){
#		print "NO $_ \n";
#		for(my $i=0; $i<=$#wapBrowserList; $i++){
#			&executeLottery($wap_addr, \@survey, $wapBrowserList[$i], $tokenList[$i]);
#		}
#	}
#
#	#获取token后并发执行抽奖
#	my $doBinfa = 0;
#	my @threadHead;
#	my @threadReturn;
#	for(1..2){
#		print "NO $_ \n";
#		for(my $i=0; $i<=$#wapBrowserList; $i++){
#			push @threadHead ,threads->create(
#				sub{
#					return &executeLottery($wap_addr, \@survey, $wapBrowserList[$i], $tokenList[$i]);
#				}
#			);
#			$doBinfa++;
#			if($doBinfa eq $bingfa){
#				for(@threadHead){
#					$_->join();
#				}
#				$doBinfa = 0;
#				@threadHead = ();
#				print "--do join--\n";
#			}
#		}
#	}



	#单次循环
#	my ($wapBrowser, $LotteryReturn);
#	my $count = 0;
#	for(1..2){
#		for my $useraccount (@userlist){
#			#计数器
#			$count++;
#			print "NO $count ";
#			#手机登录

#			next if($useraccount =~ /^#/);
#			chomp($useraccount);
#			($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
#			next unless($mobrowser);
#			print "Token:($accessToken) ";
#			
#			#逐个登录wap
#			$wapBrowser = &wapLogin($wap_addr,$accessToken);
#			print "wapBrowser:($wapBrowser) ";
#			
#			#执行抽奖接口
#			$LotteryReturn = &executeLottery($wap_addr, \@survey, $wapBrowser, $accessToken);
#			myprint("encode", "LotteryReturn:($LotteryReturn) \n");
#		}	
#	}
	
	
	
	#单次循环并发
	my $doBinfa = 0;
	my @threadHead;
	my @threadReturn;
	my ($wapBrowser, $LotteryReturn);
	my $count = 0;

	for(1..2){
		for my $index (0..$data{'maxDataLine'}){
			my $useraccount = $data{'account'}->[$index];
			chomp($useraccount);
			next if($useraccount =~ /^#/);
			#计数器
			$count++;
			print "NO $count $useraccount \n";
			#创建抽奖线程
			push @threadHead ,threads->create(
				sub{
					#手机登录
					($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
					next unless($mobrowser);
					print "Token:($accessToken) ";
					#逐个登录wap
					$wapBrowser = &wapLogin($wap_addr,$accessToken);
					print "wapBrowser:($wapBrowser) ";
					#执行抽奖接口
					$LotteryReturn = &executeLottery($wap_addr, \@survey, $wapBrowser, $accessToken);
					myprint("encode", "LotteryReturn:($LotteryReturn) \n");
				}
			);
			$doBinfa++;
			
			if($doBinfa eq $bingfa){
				for(@threadHead){
					$_->join();
				}
				$doBinfa = 0;
				@threadHead = ();
				print "--do join--\n";
			}
		}	
	}	
	
	



	return -1;

}
1;
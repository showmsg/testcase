#手机创建问卷
#use strict;

sub mobileCreateSurvey
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	
	
	#被测试的问卷id
	my $bingfa = 1;			#并发量

	
	#手机登录请求
	#'MTExMTExMTE=' => '11111111'
	#'MTIzNDU2' => 123456
	#0305
	#15086080001
	#15586080002
	#15286080003
	#15586080004
	#13486080005
	#13786080006
	#15186080007
	#13564324375
	
	#1205
	#13650650001
	#15850650002
	#13250650003
	#13550650004
	#15250650005
	#13150650006
	#13650650007


	my ($mobrowser,$accessToken) = &moblogin($mob_addr,'13650650001','MTExMTExMTE=');
	
	#这里创建问卷
	#需要读取的数据文件
	my %data = &readExcel('mobileCreateSurvey.xls','createBrief');
	my $surveyId = 0;
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		
		my @createBriefPost = ();
		push @createBriefPost ,('surveyTitle', time()."-".$data{'surveyTitle'}->[$index]);
		push @createBriefPost ,('surveyDesc', $data{'surveyDesc'}->[$index]);
		push @createBriefPost ,('categoryId', $data{'categoryId'}->[$index]);
		push @createBriefPost ,('limitedTime', $data{'limitedTime'}->[$index]);
		
		$surveyId = &createBrief($mob_addr,$mobrowser,$accessToken,\@createBriefPost);
		print "surveyId:$surveyId\n";
	}
	
	#这里创建题目
	#题目类型哈希 1单选 2多选3尺度4比一比5踩or赞6.填空题-文字 7.填空题-数字
	my %questType = (
		"1" => "单选题",
		"2" => "多选题",
		"3" => "尺度题",
		"4" => "比一比",
		"5" => "踩or赞",
		"6" => "填空题-文字",
		"7" => "填空题-数字",
	);
	
	
	my %data_1 = &readExcel('mobileCreateSurvey.xls','addQuestion');
	for my $index (0..$data_1{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		
		my @addQuestion = ();
		push @addQuestion ,('surveyId', $surveyId);
		push @addQuestion ,('questionType', $data_1{'questionType'}->[$index]);
		push @addQuestion ,('content', $data_1{'content'}->[$index]);
		push @addQuestion ,('options', $data_1{'options'}->[$index]);
		push (@addQuestion ,('questionImage', $data_1{'questionImage'}->[$index])) if($data_1{'questionImage'}->[$index]);
		
		print $questType{$data_1{'questionType'}->[$index]}."\n";
		&addQuestion($mob_addr,$mobrowser,$accessToken,\@addQuestion);
		
	}
	
	#这里是点击发布
	&onShelf($mob_addr,$mobrowser,$accessToken,$surveyId);
	
	
	return -1;
}
1;
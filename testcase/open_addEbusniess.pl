﻿use strict;


sub open_addEbusniess
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	#开放平台地址
	my $open_addr = &config("","open_addr");
	
	#初始化一些信息


	#开放平台登录请求
	my ($openbrowser);
	($openbrowser) = &openlogin($open_addr,'grshrd49@qq.com');

	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########

	#获取测试数据
	my %data = &readExcel("addEbusniessData.xls","Sheet1");
	
	#编辑请求数据
	for my $index (0..$data{'maxDataLine'}){
		
		my $name = time().&random(1,100)."ec";
		print "name  " . $name . "\n";

		my %parameter_group = (
			id => '',
			produceImg => "",
			name => $name,
			startTimeStr => $data{'startTimeStr'}->[$index],
			endTimeStr => $data{'endTimeStr'}->[$index],
			shopName => $data{'shopName'}->[$index],
			shopUrl => $data{'shopUrl'}->[$index],
			produceName => $data{'produceName'}->[$index],
			produceUnitPrice => $data{'produceUnitPrice'}->[$index],
			produceUrl => $data{'produceUrl'}->[$index],
			myFile => [$data{'myFile'}->[$index]],
			produceDesc => $data{'produceDesc'}->[$index],
			singleBackAmount => $data{'singleBackAmount'}->[$index],
			backPlaces => $data{'backPlaces'}->[$index],
			isSingelRepeatPurchase => '1',
			isParticipantQualification => '1',
			qualificationDesc => $data{'qualificationDesc'}->[$index],
			isParticipanQuestion => '1',
			questionDesc => $data{'questionDesc'}->[$index],
			isOther => '1',
			otherDesc => $data{'otherDesc'}->[$index],
			
		);
		
		#执行创建问卷函数,并返回surveyid
		print "--result--" . &addEBusiness($openbrowser,$open_addr,\%parameter_group) . " ";
		print "--expect result--" . $data{'result'}->[$index] . "\n";
	}
	
	#提交审核
	#http://10.201.2.13:8080/ec/a/submit?idStr=60
	#通过翻页获取需要strid 
	
	#通过xpath获取
	
	
	

}
1;
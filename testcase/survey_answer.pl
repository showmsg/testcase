#用户答题
#预置的答案 直接写入hash值




use strict;

sub survey_answer
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#需要读取的账号文件
	my %data = &readExcel('1411981126_10_survey_0305.xls','Sheet1');
	
	#被测试的问卷id从文件中读取
	my @resid = &readLine("data//surveyid.txt");
	my $surveyid = $resid[0];
	
	my $bingfa = 1;			#并发量
	my $answerCount = 1;	#每个用户答题次数

	
	#普通单线程模式
	#加载已经创建的用户列表
	for my $index (0..$data{'maxDataLine'}){
		next unless($data{'doit'}->[$index]);
		my $useraccount = $data{'account'}->[$index];	#用户账号
		my $myAnswerText = $data{'myAnswer'}->[$index];		#用户答题数据
		print "被测问卷id:$surveyid\n";
		
		#######
		#手机端模拟用户答题的请求
		#######
		
		#开始循环单个文件中的用户列表
		my %myAnswer = ();
		my $AnswerReturn;
		
		#处理用户答题数据将它存入hash
		if($myAnswerText =~ /^timeout$/){
			%myAnswer = ("timeout"=>"1");
		}else{
			for(split /\|/,$myAnswerText){
				my ($k,$v) = split /=/,$_;
				$myAnswer{$k} = $v;
			}
		}
		
		my $r = 1;
		for(1..$r){	#这里设置单个用户重复答题次数...
			$AnswerReturn = &userAnswer($mob_addr,\@survey,$useraccount,$surveyid,\%myAnswer);
			next unless($AnswerReturn);
		}
	}
	
	#多线程模式 - 长时间执行多线程,线程中做大循环
	#先配置多线程数据
	#加载已经创建的用户列表
#	my $bingfa = 5;
#	my %userListHash;
#	my @userListArr;
#	my @userlist;
#	for my $accfile (@acc_file){
#		if(open READ,"account//$accfile"){
#			print "account目录下 $accfile 文件成功打开\n开始读取\n";
#		}else{
#			die "account目录下无法打开 $accfile 文件\n";
#		}
#		@userlist = <READ>;
#		
#		#将测试账户打乱后存入 userlist
#		for my $useraccount (@userlist){
#			next if($useraccount =~ /^#/);
#			for(1..5){	#每个用户答题次数
#				#$userListHash{$useraccount} = 1;
#				push @userListArr,$useraccount;
#			}
#		}
#		#@userlist = keys %userListHash;
#		@userlist = @userListArr;
#		print "count".@userlist."\n";
#		
#	}
#	#根据并发量将测试账号动态分配到动态数组中
#	my $each = @userlist / $bingfa;
#	my $bf = 1;
#	for my $oneUser (@userlist){
#		push @{'bf'.$bf}, $oneUser;
#		$count++;
#		$bf++,$count=0 if($count >= $each);
#	}
#
#	#开始执行多线程
#	my %myAnswer = ();
#	my @threadHead;
#	for my $abf (1..$bingfa){
#		print "bfuser $_ ". @{'bf'.$abf} ." count\n";
#		push @threadHead ,threads->create(
#			sub{
#				for my $oneacc (@{'bf'.$abf}){
#					&userAnswer($mob_addr,\@survey,$oneacc,$surveyid,\%myAnswer);
#				}
#			}
#		);
#	}
#	for(@threadHead){
#		$_->join();
#	}

	
	
	#多线程模式 - 多线程执行单个任务 不做线程中的大循环
	#先配置多线程数据
	#加载已经创建的用户列表
#	my %userListHash;
#	my @userListArr;
#	my @userlist;
#	for my $accfile (@acc_file){
#		if(open READ,"account//$accfile"){
#			print "account目录下 $accfile 文件成功打开\n开始读取\n";
#		}else{
#			die "account目录下无法打开 $accfile 文件\n";
#		}
#		@userlist = <READ>;
#		
#		#将测试账户打乱后存入 userlist
#		for my $useraccount (@userlist){
#			next if($useraccount =~ /^#/);
#			for(1..$answerCount){	
#				#$userListHash{$useraccount} = 1;
#				push @userListArr,$useraccount;
#			}
#		}
#		#@userlist = keys %userListHash;
#		@userlist = @userListArr;
#		print "count".@userlist."\n";
#		
#	}
#
#	#开始做用户循环，到达并发量就执行并发操作 ->join()
#	#开始执行多线程
#	my $doBinfa = 0;
#	my @threadHead;
#	my %myAnswer;
#	for my $oneacc(@userlist){
#		push @threadHead ,threads->create(
#			sub{
#				&userAnswer($mob_addr,\@survey,$oneacc,$surveyid,\%myAnswer);
#			}
#		);
#		$doBinfa++;
#		if($doBinfa eq $bingfa){
#			for(@threadHead){
#				$_->detach();
#			}
#			$doBinfa = 0;
#			@threadHead = ();
#			print "--do join--\n";
#		}
#	}

	return -1;
}
1;
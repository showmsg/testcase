﻿use strict;


sub survey_question
{	
	my $number;
	($number) = @_;
	#打印说明信息
	&myprint("description",$number);
	
	#读取公共资源
	my @survey = &config("","sql");
	#管理后台地址
	my $mgt_addr = &config("","mgt_addr");
	#手机服务地址
	my $mob_addr = &config("","mob_addr");
	
	#初始化一些信息
	my @user_id = ("10301","10101","10293");
	my @username = ("15021809097","15900826057","15715559622");
	my @questid;				#存放qid
	
	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");

	########
	#创建问卷
	########
	#初始化创建问卷公用的变量
	my ($surveyid,$questionId);
	my $excelName = 'CreateSurvey.xls';

	#读取创建问卷的信息
	my %dt_1 = &readExcel($excelName, 'new_survey');
	my $partake_num = '';
	for my $index (0..$dt_1{'maxDataLine'}){
		#去除注释数据
		next unless($dt_1{'doit'}->[$index]);
		
		my @parameter_group;
		push @parameter_group,('revision', $dt_1{'revision'}->[$index]);
		push @parameter_group,('paperImage', $dt_1{'paperImage'}->[$index]);
		push @parameter_group,('awardItemImage', $dt_1{'awardItemImage'}->[$index]);
		push @parameter_group,('myfile', [$dt_1{'myfile'}->[$index]]);
		push @parameter_group,('projectCode', $dt_1{'projectCode'}->[$index]);
		push @parameter_group,('surveyTitle', time().$dt_1{'surveyTitle'}->[$index]);
		push @parameter_group,('surveyDesc', $dt_1{'surveyDesc'}->[$index]);
		push @parameter_group,('flagVideo', $dt_1{'flagVideo'}->[$index]) if($dt_1{'flagVideo'}->[$index]);
		push @parameter_group,('flagAudio', $dt_1{'flagAudio'}->[$index]) if($dt_1{'flagAudio'}->[$index]);
		push @parameter_group,('hasCargo', $dt_1{'hasCargo'}->[$index]) if($dt_1{'hasCargo'}->[$index]);
		push @parameter_group,('lotteryTypeStr', $dt_1{'lotteryTypeStr'}->[$index]);
		push @parameter_group,('quotaProportion', $dt_1{'quotaProportion'}->[$index]);
		push @parameter_group,('participationProportion', $dt_1{'participationProportion'}->[$index]);
		push @parameter_group,('presentName', $dt_1{'presentName'}->[$index]);
		push @parameter_group,('presentAmount', $dt_1{'presentAmount'}->[$index]);
		push @parameter_group,('presentCount', $dt_1{'presentCount'}->[$index]);
		push @parameter_group,('awardTypeStr', $dt_1{'awardTypeStr'}->[$index]);
		push @parameter_group,('awardAmount', $dt_1{'awardAmount'}->[$index]);
		
		#答题后抽奖的配置
		push @parameter_group,('auditPassedAmount', $dt_1{'auditPassedAmount'}->[$index]);
		push @parameter_group,('sunnySwitch', $dt_1{'sunnySwitch'}->[$index]);
		push @parameter_group,('sunnyAmountHighest', $dt_1{'sunnyAmountHighest'}->[$index]);
		push @parameter_group,('sunnyAmountLowest', $dt_1{'sunnyAmountLowest'}->[$index]);
		
		push @parameter_group,('averageAward', $dt_1{'averageAward'}->[$index]);
		push @parameter_group,('sigelHighAward', $dt_1{'sigelHighAward'}->[$index]);
		push @parameter_group,('sigelLowAward', $dt_1{'sigelLowAward'}->[$index]);
		push @parameter_group,('awardItemName', $dt_1{'awardItemName'}->[$index]);
		push @parameter_group,('awardItemPrice', $dt_1{'awardItemPrice'}->[$index]);
		push @parameter_group,('awardItemNum', $dt_1{'awardItemNum'}->[$index]);
		push @parameter_group,('awardItemDesc', $dt_1{'awardItemDesc'}->[$index]);
		push @parameter_group,('awardImagefile', [$dt_1{'awardImagefile'}->[$index]]);
		push @parameter_group,('surveyHelp', $dt_1{'surveyHelp'}->[$index]);
		push @parameter_group,('needFilter', $dt_1{'needFilter'}->[$index]) if($dt_1{'needFilter'}->[$index]);;
		push @parameter_group,('flagBooking', $dt_1{'flagBooking'}->[$index]);
		push @parameter_group,('flagIdentityAuth', $dt_1{'flagIdentityAuth'}->[$index]);
		push @parameter_group,('flagPhoneAuth', $dt_1{'flagPhoneAuth'}->[$index]);
		push @parameter_group,('rad_partake', $dt_1{'rad_partake'}->[$index]);
		push @parameter_group,('partake_num', $dt_1{'partake_num'}->[$index]);

		$partake_num = $dt_1{'partake_num'}->[$index];
		
		#test
		#my $n = 0;
		#for(@parameter_group){
		#	myprint("encode", " ($_) ");
		#	$n++;
		#	if($n eq 2){
		#		$n=0;
		#		print "\n" ;
		#	}
		#}
		#print "-----------\n";
		
		#执行创建问卷函数,并返回surveyid
		$surveyid = &addsurvey($mgtbrowser,$mgt_addr,\@parameter_group);
		
		#将surveyid写入文件
		open SID, ">data//surveyid.txt";
		print SID $surveyid;
		close SID;
	}
	
	#初始化题号
	my $sortedNum = 1;
	
	#单选题 人口学题 类型 1 20
	#读取创建单选题的信息
	my %dt_2 = &readExcel($excelName, 'module-radio');
	for my $index (0..$dt_2{'maxDataLine'}){
		#去除注释数据
		next unless($dt_2{'doit'}->[$index]);
		
		#人口学题
		if($dt_2{'helpType'}->[$index] eq "1"){
			myprint("encode","第".$sortedNum."题,人口学题\n");
			#组装创建单选题的post报文
			my @create;
			push @create, ('surveyId', $surveyid);
			push @create, ('content', $dt_2{'content'}->[$index]);
			push @create, ('type', "20");
			push @create, ('sortedNum', $sortedNum);
			push @create, ('id', "");
			push @create, ('questionFile', [$dt_2{'questionFile'}->[$index]]);
			push @create, ('questionImage', $dt_2{'questionImage'}->[$index]);
			push @create, ('allowWord', $dt_2{'allowWord'}->[$index]);
			push @create, ('show', $dt_2{'show'}->[$index]) if($dt_2{'show'}->[$index]);
			push @create, ('questionDescStatus', $dt_2{'questionDescStatus'}->[$index]) if($dt_2{'questionDescStatus'}->[$index]);
			push @create, ('questionDesc', $dt_2{'questionDesc'}->[$index]);
			push @create, ('questionDescFile', [$dt_2{'questionDescFile'}->[$index]]);
			push @create, ('questionDescPic', $dt_2{'questionDescPic'}->[$index]);
			push @create, ('helpType', $dt_2{'helpType'}->[$index]);
			push @create, ('ref', $dt_2{'ref'}->[$index]);
			push @create, ('demographics', $dt_2{'demographics'}->[$index]);
			push @create, ('optionsText', $dt_2{'optionsText'}->[$index]);
			#最终生成题目
			$questionId = &create($mgtbrowser,$mgt_addr,\@create);
			push @questid,$questionId;
			#序号增加
			$sortedNum++;
		}
		
		
		#普通的单选题
		if($dt_2{'helpType'}->[$index] eq "0"){
			myprint("encode","第".$sortedNum."题,单选题\n");
			#预处理optionsText  用%0A组装optionsText值
			my $optionsText = $dt_2{'optionsText'}->[$index] ;
			chomp($optionsText);
			$optionsText =~ s/\s+/%0A/g;
			
			#组装"生成"的字符串
			my $addOT_GET;
			$addOT_GET = $addOT_GET . "surveyId=" . $surveyid ."&";
			$addOT_GET = $addOT_GET . "optionsText=" . $optionsText . "&";
			$addOT_GET = $addOT_GET . "questionType=" . "1" . "&";
			$addOT_GET = $addOT_GET . "content=" . $dt_2{'content'}->[$index] . "&";
			$addOT_GET = $addOT_GET . "sortedNum=" . $sortedNum . "&";
			$addOT_GET = $addOT_GET . "questionId=" . "&";
			$addOT_GET = $addOT_GET . "awardWord=" . $dt_2{'allowWord'}->[$index] . "&" if($dt_2{'allowWord'}->[$index]);
			#myprint("encode","-- ($addOT_GET) --\n");
			
			#发送"生成"请求
			$questionId = &addOptionsText($mgtbrowser,$mgt_addr,$addOT_GET);
			#print "questionId  =  $questionId\n;";
			
			#组装创建单选题的post报文
			my @create;
			push @create, ('surveyId', $surveyid);
			push @create, ('content', $dt_2{'content'}->[$index]);
			push @create, ('type', "1");
			push @create, ('sortedNum', $sortedNum);
			push @create, ('id', $questionId);
			push @create, ('questionFile', [$dt_2{'questionFile'}->[$index]]);
			push @create, ('questionImage', $dt_2{'questionImage'}->[$index]);
			push @create, ('allowWord', $dt_2{'allowWord'}->[$index]);
			push @create, ('show', $dt_2{'show'}->[$index]) if($dt_2{'show'}->[$index]);
			push @create, ('questionDescStatus', $dt_2{'questionDescStatus'}->[$index]) if($dt_2{'questionDescStatus'}->[$index]);
			push @create, ('questionDesc', $dt_2{'questionDesc'}->[$index]);
			push @create, ('questionDescFile', [$dt_2{'questionDescFile'}->[$index]]);
			push @create, ('questionDescPic', $dt_2{'questionDescPic'}->[$index]);
			push @create, ('helpType', $dt_2{'helpType'}->[$index]);
			push @create, ('ref', $dt_2{'ref'}->[$index]);
			push @create, ('demographics', $dt_2{'demographics'}->[$index]);
			push @create, ('optionsText', $dt_2{'optionsText'}->[$index]);
			#动态组装题目选项以及图片地址
			my $optionSheetName = $dt_2{'optionFileRef'}->[$index];
			my %optionInfo = &readExcel($excelName, $optionSheetName);
			for my $index (0..$optionInfo{'maxDataLine'}){
				push @create, ("options[$index].text", $optionInfo{'optionsTextN'}->[$index]);
				push @create, ("optionFile$index", [$optionInfo{'optionFileN'}->[$index]]);
				push @create, ("options[$index].imageUrl", "");
			}
			#最终生成题目
			$questionId = &create($mgtbrowser,$mgt_addr,\@create);
			push @questid,$questionId;
			#序号增加
			$sortedNum++;
		}
		
	}
	
	#多选题 类型 2
	my %dt_3 = &readExcel($excelName, 'module-checkbox');
	for my $index (0..$dt_3{'maxDataLine'}){
		#去除注释数据
		next unless($dt_3{'doit'}->[$index]);
				
		#普通的多选题if是便于将来可能的扩展
		if(1){
			myprint("encode","第".$sortedNum."题,多选题\n");
			#预处理optionsText  用%0A组装optionsText值
			my $optionsText = $dt_3{'optionsText'}->[$index] ;
			chomp($optionsText);
			$optionsText =~ s/\s+/%0A/g;
			
			#组装"生成"的字符串
			my $addOT_GET;
			$addOT_GET = $addOT_GET . "surveyId=" . $surveyid ."&";
			$addOT_GET = $addOT_GET . "optionsText=" . $optionsText . "&";
			$addOT_GET = $addOT_GET . "questionType=" . "2" . "&";
			$addOT_GET = $addOT_GET . "content=" . $dt_3{'content'}->[$index] . "&";
			$addOT_GET = $addOT_GET . "sortedNum=" . $sortedNum . "&";
			$addOT_GET = $addOT_GET . "questionId=" . "&";
			$addOT_GET = $addOT_GET . "awardWord=" . $dt_3{'allowWord'}->[$index] . "&" if($dt_3{'allowWord'}->[$index]);
			#myprint("encode","-- ($addOT_GET) --\n");
			
			#发送"生成"请求
			$questionId = &addOptionsText($mgtbrowser,$mgt_addr,$addOT_GET);
			#print "questionId  =  $questionId\n;";
			
			#组装创建单选题的post报文
			my @create;
			push @create, ('surveyId', $surveyid);
			push @create, ('content', $dt_3{'content'}->[$index]);
			push @create, ('type', "2");
			push @create, ('sortedNum', $sortedNum);
			push @create, ('id', $questionId);
			push @create, ('questionFile', [$dt_3{'questionFile'}->[$index]]);
			push @create, ('questionImage', $dt_3{'questionImage'}->[$index]);
			push @create, ('allowWord', $dt_3{'allowWord'}->[$index]);
			push @create, ('show', $dt_3{'show'}->[$index]) if($dt_3{'show'}->[$index]);
			push @create, ('questionDescStatus', $dt_3{'questionDescStatus'}->[$index]) if($dt_3{'questionDescStatus'}->[$index]);
			push @create, ('questionDesc', $dt_3{'questionDesc'}->[$index]);
			push @create, ('questionDescFile', [$dt_3{'questionDescFile'}->[$index]]);
			push @create, ('questionDescPic', $dt_3{'questionDescPic'}->[$index]);
			push @create, ('maxOptional', $dt_3{'maxOptional'}->[$index]);
			push @create, ('optionsText', $dt_3{'optionsText'}->[$index]);
			#动态组装题目选项以及图片地址
			my $optionSheetName = $dt_3{'optionFileRef'}->[$index];
			my %optionInfo = &readExcel($excelName, $optionSheetName);
			for my $index (0..$optionInfo{'maxDataLine'}){
				push @create, ("options[$index].text", $optionInfo{'optionsTextN'}->[$index]);
				push @create, ("optionFile$index", [$optionInfo{'optionFileN'}->[$index]]);
				push @create, ("options[$index].imageUrl", "");
			}
			
			#最终生成题目
			$questionId = &create($mgtbrowser,$mgt_addr,\@create);
			push @questid,$questionId;
			#序号增加
			$sortedNum++;
		}
		
	}
	
	
	#文本输入 类型 5.文本 4.数字 13.邮箱 16.手机 18.日期时间
	my %dt_4 = &readExcel($excelName, 'module-text');
	for my $index (0..$dt_4{'maxDataLine'}){
		#去除注释数据
		next unless($dt_4{'doit'}->[$index]);
				
		#普通的多选题if是便于将来可能的扩展
		if(1){
			myprint("encode","第".$sortedNum."题,多选题\n");
			#预处理optionsText  用%0A组装optionsText值
			my $optionsText = $dt_3{'optionsText'}->[$index] ;
			chomp($optionsText);
			$optionsText =~ s/\s+/%0A/g;
						
			#组装创建单选题的post报文
			my @create;
			push @create, ('surveyId', $surveyid);
			push @create, ('content', $dt_3{'content'}->[$index]);
			push @create, ('questionFile', [$dt_3{'questionFile'}->[$index]]);
			push @create, ('questionImage', $dt_3{'questionImage'}->[$index]);
			push @create, ('sortedNum', $sortedNum);
			push @create, ('id', $questionId);
			push @create, ('show', $dt_3{'show'}->[$index]) if($dt_3{'show'}->[$index]);
			push @create, ('questionDescStatus', $dt_3{'questionDescStatus'}->[$index]) if($dt_3{'questionDescStatus'}->[$index]);
			push @create, ('questionDesc', $dt_3{'questionDesc'}->[$index]);
			push @create, ('questionDescFile', [$dt_3{'questionDescFile'}->[$index]]);
			push @create, ('questionDescPic', $dt_3{'questionDescPic'}->[$index]);
			push @create, ('type', $dt_3{'type'}->[$index]);
			push @create, ('validation', $dt_3{'validation'}->[$index]);
			push @create, ('repeat', $dt_3{'repeat'}->[$index]);
			push @create, ('format', $dt_3{'format'}->[$index]);
			push @create, ('now', $dt_3{'now'}->[$index]);
		

			
			#最终生成题目
			$questionId = &create($mgtbrowser,$mgt_addr,\@create);
			push @questid,$questionId;
			#序号增加
			$sortedNum++;
		}
		
	}
	
	
	#########
	#逻辑设置 or and to end  select noselect
	#########
	my @logicList = (
		"q2-select-1 or q2-select-3 end",
	);
	#&modify($mgtbrowser,$mgt_addr,$surveyid,\@logicList,\@questid);



	########
	#创建配额
	########
	#1女  0男
	my (@quota_1, @quota_2);
	#提交一个
	@quota_1 = (
		'age',"",
		'amount',"",
		'area',"",
		'reviewer','213',
		'reviewer','213',
		'sex','',
		'surveyCount','200',
		'surveyId',$surveyid,
		);
	@quota_2 = (
		'quotaType',"1",
		'totalSamples',"",
		'surveyId',$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,$surveyid,\@quota_1,\@quota_2);

	####
	#提交问卷
	#survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
#	####
#	#审核上家
#	####
#	my %auditconfig = (
#		partakeNum => $partake_num,
#	);
#	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
#
#	####
#	#立刻上架操作
#	####
#	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","20");
#	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
#	&operate_mysql(@survey,"$sql","update");
#
#	#}
#	#######
#	#手机端验证是否存在该问卷
#	#######
#	return &checkMySurvey($mob_addr,$surveyid,$username[0]);

}
1;
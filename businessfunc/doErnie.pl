﻿#模拟用户开始答题

use strict;

sub doErnie
{
	my ($mob_addr,$surveyStr,$mobrowser,$accessToken,$sunnyId) = @_;	
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	#开始抽奖
	my $returnJson_2 = $mobrowser->post(
		"$mob_addr/survey-ws/surveySunny/doErnie",
		{
			sunnyId => $sunnyId,
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	unless (&verification("mob","doErnie",$mobrowser,$returnJson_2)){
		return 0;
	}
	$json_obj = $json->decode($returnJson_2);
	my $amount = $json_obj->{'data'}->{'amount'};
	

	return $amount;
}
1;
﻿#创建题目
#myprint("encode", "现实内容");
use strict;
####
#问卷题目编辑函数
####
sub editQuest
{
	my ($openbrowser,$open_addr,$questType_HashRef,$questInfo_ArrRef,$Options_ArrRef) = @_;
	my @editQuestPost;
	
	#初始化数据
	my $type = $questInfo_ArrRef->{'type'};
	my $content = shift @$Options_ArrRef;		#这是把Options_ArrRef的0标题
	my $contenpPic = shift @$Options_ArrRef;	#和0标题的配图路径取出来

	#myprint("encode", "$content,$contenpPic \n");
	
	#组装统一的post数据
	push @editQuestPost,('id',"");
	push @editQuestPost,('type',$type);
	push @editQuestPost,("surveyId",$questInfo_ArrRef->{'surveyid'});
	push @editQuestPost,("sortedNum",$questInfo_ArrRef->{'sort'});
	push @editQuestPost,('content',$content);
	push @editQuestPost,('questionFile',[$contenpPic]);
	push @editQuestPost,('questionImage',"");
	
	#区别类型,并组装post数据
	if($type eq '1' || $type eq '2'){	#单多选
		my $start = '0';
		for(my $i=0; $i<$#$Options_ArrRef; $i+=2){
			my $nextI = $i + 1; 
			push @editQuestPost,("options[$start].text", $Options_ArrRef->[$i]);		#选项标题
			push @editQuestPost,("optionFile".$start , [$Options_ArrRef->[$nextI]]);	#选项图片
			push @editQuestPost,("options[".$start."].imageUrl" , "");					#未知参数
			$start++;
		}
		push @editQuestPost,("allowWord" , $questType_HashRef->{allowWord});
	}
	
	if($type eq '3'){	#处理尺度题
		push @editQuestPost,("scaleNum" , $Options_ArrRef->[1]);		#选项标题
		push @editQuestPost,("leftDesc" , $Options_ArrRef->[3]);	#选项图片
		push @editQuestPost,("rightDesc" , $Options_ArrRef->[5]);					#未知参数
	}
	
	if($type eq '4' || $type eq '5'){	#数字 文字
		#似乎数字题文字题没什么好做的....
	}

	if($type eq ''){	#以后开放平台有更多的操作....
	
	}

	

#	for(my $i=0; $i<$#editQuestPost; $i+=2){
#		my $nextI = $i + 1; 
#		myprint("encode", "$editQuestPost[$i] -- $editQuestPost[$nextI]\n");
#	}
	
	#开始提交请求
	my $return_json;
	$return_json = $openbrowser->post("$open_addr/survey/a/editQuest",{@editQuestPost},
		'Content-Type'=>"multipart/form-data;",
	)->content;
	#print "--debug--创建问卷 $return_json\n";
	&verification("mgt","editSurvey",$openbrowser,$return_json);
	
	my $questid;
	if($return_json =~ /\"id\":\"(.*?)\"/){
		$questid = $1;
	}
	if($questid eq ""){
		die "Create quest failure,questId is null,stop script\n";
	}else{
		myprint("encode", "Create question success,question is $questid,question type is ". $questType_HashRef->{$type} ."\n");
		return $questid;
	}
}
1;
#获取配额中需要对比的字段

use strict;

sub get_quota_data
{
	my ($surveyid,$dbh) = @_;
	
	my $sqlword = "SELECT PREDICATE_DATA FROM SURVEY_QUOTA_SETTING WHERE SURVEY_ID=$surveyid";
	my @predicate_data = &operate_mysql_new($dbh,"$sqlword","select");
	
	#创建json对象 一边处理返回的字符串
	my $json = JSON->new->utf8;
	my $json_obj;
	#$json_obj = $json->decode("$return");	#把返回的json数组传入Json
	#@{$json_obj->{'data'}->{'surveys'}}
	
	my %return_hash;	#用户返回配额字段
	my @arr_hash;
	for my $return_data (@predicate_data){
		$json_obj = $json->decode("$return_data");
		for my $key (keys $json_obj){
			$return_hash{$key}=1;
			#print "debug-- $key : $json_obj{$key} \n";
		}
	}
	for(keys %return_hash){
		push @arr_hash,$_;
		#print "debug-- $_ \n";
	}
	
	#处理需要的统计字段
	#将需要处理的字段塞入数组,塞入的字符串用数据库user_profile对应的字段名
	my @commandarr;
	my %datahash;	#存入数据库查处的数据，以便和下载的文件做对比
	for(@arr_hash){
		next if($_ eq "location");	#地区
		
		if($_ eq "gender"){				#性别有对应字段可以直接操作
			my $sqlword = "SELECT SEX FROM USER_PROFILE WHERE USER_ID IN 
				(SELECT USER_ID FROM `USER_SURVEY` WHERE SURVEY_ID=$surveyid AND USER_SURVEY_STATUS=4) ";
			my @sex = &operate_mysql_new($dbh,$sqlword,"select");
			for(@sex){
				if($_ eq 0){
					$datahash{"性别_男"}++;
					next;
				}
				if($_ eq 1){
					$datahash{"性别_女"}++;
				}
			}
			next;
		}
		
		if($_ eq "age"){					#年龄没有对应字段需要进一步操作
			&get_agelist();
			next;
		}
		
		if($_ eq "marriage"){		#婚姻有对应字段可以直接操作
			&getdata("marriage","MARRIAGE");
			next;
		}
		if($_ eq "education"){		#教育有对应字段可以直接操作
			&getdata("education","EDUCATION");
			next;
		}
		if($_ eq "profession"){	#职业有对应字段可以直接操作
			&getdata("profession","PROFESSION");
			next;
		}
		if($_ eq "vocation"){		#行业有对应字段可以直接操作
			&getdata("vocation","VOCATION");
			next;
		}
		if($_ eq "position"){		#职位有对应字段可以直接操作	
			&getdata("position","POSITION");
			next;
		}
		if($_ eq "income"){			#月收入有对应字段可以直接操作
			&getdata("income","INCOME");
			next;
		}
		if($_ eq "incomefamily"){	#家庭月收入有对应字段可以直接操作
			&getdata("incomefamily","INCOME_FAMILY");
			next;
		}

	}
	
	
	#foreach my $key (sort {$a<=>$b} keys %datahash){  
	#	print "return_sql  --  key:$key  value:$datahash{$key} \n";
	#}

	return %datahash;
	
	sub get_agelist
	{
		my $sqlword = "SELECT YEAR(BIRTHDAY) FROM USER_PROFILE WHERE USER_ID IN 
			(SELECT USER_ID FROM `USER_SURVEY` WHERE SURVEY_ID=$surveyid AND USER_SURVEY_STATUS=4) ";
		my @birthday_yearlist = &operate_mysql_new($dbh,$sqlword,"select");
		#提取所有的birthdaylist中的年份
		my $thisyear = &nowtime("1","","","","","","");
		#得到每个用户年龄
		for(@birthday_yearlist){
			$_ = $thisyear - $_;
		}
		
		
		#查询数据库获取配额的年龄列表
		$sqlword = "SELECT PREDICATE_DATA FROM SURVEY_QUOTA_SETTING WHERE SURVEY_ID=$surveyid";
		my @jsonlist = &operate_mysql_new($dbh,$sqlword,"select");
		for my $jsonsrt (@jsonlist){
			$json_obj = $json->decode("$jsonsrt");
			next unless($json_obj->{'age'});
			for my $age (@{$json_obj->{'age'}}){
				#这里返回 SURVEY_QUOTA_SETTING表中PREDICATE_DATA字段中的 每个配额的年龄列表
				my @quota_age = @{$age->{'dstValue'}};
				#组装年龄列表的名字，如[10-20]
				my $age_list_name = "[".$quota_age[0]."-".$quota_age[$#quota_age]."]";
				#计算出该年龄段的总数,并塞入数组@age_arr
				my $age_temp_count = 0;
				for(@birthday_yearlist){
					$datahash{"年龄_".$age_list_name}=++$age_temp_count if($_>=$quota_age[0] && $_<=$quota_age[$#quota_age]);
				}
			}
		}
		
		
	}
	
	sub getdata
	{
		#有对应字段可以直接操作的统计指标插入对比hash中
		my ($command,$field) = @_;
		
		my $sqlword = "SELECT $field FROM USER_PROFILE WHERE USER_ID IN 
			(SELECT USER_ID FROM `USER_SURVEY` WHERE SURVEY_ID=$surveyid AND USER_SURVEY_STATUS=4) ";
		my @list = &operate_mysql_new($dbh,$sqlword,"select");
		my %indexhash = &quota_index($command);	#这里返回配额模板		
		for(@list){
			#print "----debug----marriagelist---".$indexhash{$_} ."\n";
			#print "$_\n" if($command eq "profession" && $_ eq "11");
			#print "$indexhash{$_}\n" if($indexhash{$_} eq "其他");
			$datahash{$indexhash{$_}}++;
		}
		
		#print "$command--其他 : ".$datahash{"其他"}."\n";
		#调试
		#if($field == "MARRIAGE"){
		#	print "----debug----marriage---".@list ."\n";
		#}
		
	}

}
1;
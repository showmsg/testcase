#功能：执行微报告下架操作
#http://10.201.70.81/survey/reports/put_off_shelf/1821

sub put_off_shelf
{
	#参数说明(http的方法类, 管理后台的地址, addsurvey的post参数群(传入的是引用))
	my ($mgtbrowser,$mgt_addr,$reportId) = @_;

	my $return_json;
	$return_json = $mgtbrowser->get("$mgt_addr/survey/reports/put_off_shelf/$reportId")->content;

	return &verification("mgt","addsurvey",$mgtbrowser,$return_json);
	
}
1;
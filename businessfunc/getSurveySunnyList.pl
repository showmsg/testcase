﻿#模拟用户开始答题

use strict;

sub getSurveySunnyList
{
	my ($mob_addr, $surveyStr, $mobrowser, $accessToken, $surveyid) = @_;

	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	#先从surveyid获取sunnyId
	my $sunnyId = '';
	my $returnJson_1 = $mobrowser->get(
		"$mob_addr/survey-ws/surveySunny/getSurveySunnyList",
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
#	&myprint("encode","$returnJson_1");
	
	unless (&verification("mob","getSurveySunnyList",$mobrowser,$returnJson_1)){
		return 0;
	}
	$json_obj = $json->decode($returnJson_1);
	for my $eachone (@{$json_obj->{'data'}}){
		if($eachone->{'surveyId'} eq $surveyid){
			$sunnyId = $eachone->{'id'};
			last;
		}
	}
	
	
	return $sunnyId;
}
1;
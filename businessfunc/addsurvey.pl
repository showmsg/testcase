#功能：管理后台创建问卷addsurvey接口的请求
#http://10.201.70.81/survey/create

sub addsurvey
{
	#参数说明(http的方法类, 管理后台的地址, addsurvey的post参数群(传入的是引用))
	my ($mgtbrowser,$mgt_addr,$parameter_group_ref) = @_;
	#创建问卷addsurvey
	my $return_json;
	$return_json = $mgtbrowser->post("$mgt_addr/survey/create",
		{	
			@$parameter_group_ref
		},
		'Referer'=>'$mob_addr/survey/new_survey',
		'Content-Type'=>"multipart/form-data;",
	)->content;
	#print "--debug--创建问卷 $return_json\n";
	&verification("mgt","addsurvey",$mgtbrowser,$return_json);
	
	my $surveyId;
	if($return_json =~ /\"surveyId\":\"(.*?)\"/){
		$surveyId = $1;
	}
	if($surveyId eq ""){
		die "Create survey failure,suvrveyId is null,stop script\n";
	}else{
		print "Create survey success,suvrveyId is $surveyId\n";
		return $surveyId;
	}
}
1;
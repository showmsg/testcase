﻿#mode=2 将返回的字符串做二进制处理
use strict;

sub download_file
{
	my ($mgtbrowser , $url , $mode) = @_;
	
	#my $url = "$mgt_addr/check/downLoadSamples/3625/samples";
	my $filename;
	my $Content_Disposition;
	my $Content_Length;
	my $res;
	
	#初始化请求方法
	&myprint("encode", "开始下载测试文件....\n");
	$res = $mgtbrowser->request(new HTTP::Request(GET=>$url));

	#得到相应头中的文件名
	$Content_Disposition = $res->header("Content-Disposition");
	&myprint("encode", "filename : $Content_Disposition\n");
	if($Content_Disposition =~ /.*fileName=(.*)/i){
		$filename = $1;
	}
	$filename = encode("gbk", decode("utf-8", $filename));
	print "filename : $filename\n";
	
	$Content_Length = $res->header("Content_Length");
	&myprint("encode", "Content_Length : $Content_Length\n");

	
	my $return = $res->content;
	open XLS,">$filename";
	binmode(XLS) if($mode == 2);
	
	#print XLS  pack( 'H2', $return );
	print XLS $return;
	close XLS;
	
	&myprint("encode", "测试文件下载完毕 >$Content_Disposition<\n");
	
	return $filename if($mode == 2); 
	return $return;
}
1;
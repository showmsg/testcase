#功能：管理后台创建问卷addsurvey接口的请求
#http://10.201.70.81/survey/create
use strict;

sub editSurvey
{
	#参数说明(http的方法类, 管理后台的地址, addsurvey的post参数群(传入的是引用))
	my ($openbrowser,$open_addr,$parameter_group_ref) = @_;
	
	#将参数群传入数组
	my %parameter_group = %$parameter_group_ref;
	#创建问卷addsurvey
	#myFile=>["survey_title.jpg"], 上传文件的参数要用[]阔起来
	my $return_json;
	$return_json = $openbrowser->post("$open_addr/survey/a/editSurvey",
		{	
			%parameter_group
		},
		'Content-Type'=>"multipart/form-data;",
	)->content;
	#print "--debug--创建问卷 $return_json\n";
	&verification("mgt","editSurvey",$openbrowser,$return_json);
	
	my $surveyId;
	if($return_json =~ /\"surveyId\":\"(.*?)\"/){
		$surveyId = $1;
	}
	if($surveyId eq ""){
		die "Create survey failure,suvrveyId is null,stop script\n";
	}else{
		print "Create survey success,suvrveyId is $surveyId\n";
		return $surveyId;
	}
}
1;
﻿#模拟用户开始答题

use strict;

sub checkUserInfo
{
	my ($mob_addr,$surveyStr,$useraccount,$surveyid) = @_;

	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;

	chomp($useraccount);
	my ($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount);
	
	print "$useraccount-";
	#刮奖
	my $returnJson = $mobrowser->post("$mob_addr/survey-ws/surveysvc/scratch",
		{
			surveyId => $surveyid,
		},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	#print "debug --- $returnJson\n";
	&verification("mob","checkUserInfo.scratch",$mobrowser,$returnJson);
	
	$json_obj = $json->decode("$returnJson");
	my $id = $json_obj->{'data'}->{'id'};
	my $bonus = $json_obj->{'data'}->{'bonus'};
	my $rewardNum = $json_obj->{'data'}->{'rewardNum'};
	my $notRewardNum = $json_obj->{'data'}->{'notRewardNum'};
	my $maxReward = $json_obj->{'data'}->{'maxReward'};
	my $minReward = $json_obj->{'data'}->{'minReward'};
	my $rewardTip = $json_obj->{'data'}->{'rewardTip'};
	
	#查看资料
	my $returnJson = $mobrowser->get("$mob_addr/survey-ws/usersvc/personalCenter",
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	#print "debug --- $returnJson\n";
	&verification("mob","checkUserInfo.userCenter",$mobrowser,$returnJson);
	
	$json_obj = $json->decode("$returnJson");
	my $currentAmount = $json_obj->{'data'}->{'currentAmount'};
	
	if($rewardTip =~ /.*?(\d+\.?\d*).*?/){
		$rewardTip = $rewardTip;
	}
	
	my $p = "$rewardTip - $currentAmount\n";
	$p = encode("gbk", $p);
	print $p;
	#&myprint("encode","$useraccount - $id - $rewardTip - $currentAmount\n");
	
	
}
1;
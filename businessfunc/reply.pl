﻿#模拟用户开始答题

use strict;

sub reply
{
	my ($mob_addr, $surveyStr, $mobrowser, $accessToken, $postReply) = @_;	
	
	my %postReply = ();
	$postReply{'contentId'} = $postReply->[0] if($postReply->[0]);
	$postReply{'contentType'} = $postReply->[1] if($postReply->[1]);
	$postReply{'rootId'} = $postReply->[2] if($postReply->[2]);
	$postReply{'commentId'} = $postReply->[3] if($postReply->[3]);
	$postReply{'content'} = $postReply->[4] if($postReply->[4]);
	$postReply{'imgUrls'} = $postReply->[5] if($postReply->[5]);
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	#开始抽奖
	my $returnJson_2 = $mobrowser->post(
		"$mob_addr/survey-ws/surveysvc/wonder/reply",{%postReply},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	unless (&verification("mob","reply",$mobrowser,$returnJson_2)){
		return 0;
	}
	$json_obj = $json->decode($returnJson_2);
	my $rewardPoints = $json_obj->{'data'}->{'rewardPoints'};

	

	return ($rewardPoints);
}
1;
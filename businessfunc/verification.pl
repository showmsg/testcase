﻿#验证函数

sub verification
{
	my ($proname,$subname,$browser,$returninfo) = @_;
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	if($proname eq "mgt"){
		#这个验证mgt项目的请求返回值
		#返回的ftl页面报错标识符 The failing instruction
		if(&judgeJson($returninfo)){	#先判断是否是json数组
			#返回的状态值验证
			$json_obj = $json->decode("$returninfo");
			#获取状态值
			$status = $json_obj->{'status'};
		
			if($status<=0){
				myprint("encode" , "--error.status-- subname:$subname -- $returninfo\n");
				print LOG "--error.status-- subname:$subname -- $returninfo\n";
				return &option(0);
			}else{
				print LOG "$returninfo\n";
				return &option(1);
			}
		}
		
		#返回的页面,主要验证ftl的错误页面
		if($returninfo  =~ /The failing instruction/i){
			myprint("encode" , "--error.ftl-- subname:$subname -- $returninfo\n");
			print LOG "--error.status-- subname:$subname -- $returninfo\n";
			return &option(0);
		}else{
			print LOG "$returninfo\n";
			return &option(1);
		}
	}
	
	if($proname eq "mob"){
		#这个验证手机的请求
		#先判断是否是json
		unless(&judgeJson($returninfo)){
			myprint("encode",  "--error.mobServer return is no json-- subname:$subname -- $returninfo\n");
			return 4;
		}
		
		#获取结果值
		$json_obj = $json->decode("$returninfo");
		my $result = $json_obj->{'result'};
		
		if($result eq "0" || $result eq "1"){
			print LOG "$returninfo\n";
			return &option(1);
		}
				
		if($result eq "2"){
			#进一步区分是何种异常根据message消息
			my $message = $json_obj->{'data'}->{'message'};
			$message = encode("utf-8", $message);
			myprint("encode",  "--error.result-- subname:$subname -- $returninfo\n");
			print LOG "--error.result-- subname:$subname -- $returninfo\n";
			if($message eq '手机号和昵称都已被注册！'){
				print "--debug retu 3  --- \n";
				return 3;
			}
			
			return &option(0);
		}

	}
	
	die "Verify failure,Can not found this proname \"$proname\",subname \"$subname\"\n";
	
	sub judgeJson
	{
		#靠前后的{}来判断是否是json
		my ($jsonwd) = @_;
		if($jsonwd =~ /^{.*?}$/){
			return 1;
		}else{
			return 0;
		}
	}
	
	sub option
	{
		#遇到异常后提示用户是否要终止测试
		my ($ctrl) = @_;
		unless($ctrl){
			print "Return Value Exception,enter \"s\" stop script,anykey contiune!!\n";
			#chomp(my $enter = <STDIN>);
			#die "stop script\n" if($enter eq "s");
			return 0;
		}
		return 1;
	}
}
1;
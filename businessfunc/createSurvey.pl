﻿#一个完成的创建问卷的过程，包含上架动作
#参数包含逻辑配置，对应跳转测试，用例编号16 17 18 19

use strict;

sub createSurvey_1
{
	#$logichash 不用转成数组直接进逻辑设置接口在转数组
	my ($mgt_addr,$description,$questionType) = @_;
	
	
	myprint("encode", "开始创建测试问卷\n");
	
	#配额
	#my $surveyCount_1 = "20"; #预设值,如果没有赋值需要用户输入
	#if($surveyCount_1 eq ""){
	#	&myprint("encode", "问卷的配额总量? ");
	#	chomp(my $surveyCount_1 = <STDIN>);
	#}
	&myprint("encode", "开始创建问卷,预置配额:20\n");
	
	#管理后台登录请求
	my ($mgtbrowser);
	($mgtbrowser) = mgtlogin($mgt_addr,"yangxianming1");
	#($mgtbrowser) = mgtloginForHttps($mgt_https,"yangxianming1");
	########
	#测试代码(这里开始编写主要测试逻辑业务)
	########
	
	########
	#创建问卷
	########
	#初始化数据
	my %parameter_group = (
		paperImage=>"",
		awardItemImage=>"",
		myfile=>"",
		projectCode=>"perltest",
		surveyTitle=>time()."_perlscript_$description",
		surveyDesc=>"perlscript create survey--$description",
		flagVideo=>"1",				#带视频(1.带视频  空:不带视频)
		hasCargo=>"0",				#物流(1.需要  0.不需要)
		lotteryTypeStr=>'0',
		quotaProportion=>'',
		participationProportion=>'',
		presentName=>"",
		presentAmount=>"",
		presentCount=>"",
		awardTypeStr=>"1",			#1.现金 2.非现金 3.红包
		awardAmount=>"1",
		averageAward=>"0",
		sigelHighAward=>"0",
		sigelLowAward=>"0",
		awardItemName=>"",
		awardItemPrice=>"",
		awardItemNum=>"",
		awardItemDesc=>"",
		awardImagefile=>"",
		surveyHelp=>"",
		flagBooking=>"0",			#是否是未来任务(1.是  0.否)
		flagIdentityAuth=>"",		#需要身份验证(1.需要  空:不需要)
		flagPhoneAuth=>"",			#需要手机验证(1.需要  空:不需要)
	);
	#执行创建问卷函数,并返回surveyid
	my $surveyid = &addsurvey($mgtbrowser,$mgt_addr,\%parameter_group);
	
	
	
	##############################
	#创建edit_question
	#尺度题数组@Options格式：("尺度大小3|5|7|9","左描述","右描述")
	#数字填空，文字填空，需要清空数组@Options
	#视频题@Options格式：("视频文件的本地地址")
	#将数组传入createquest函数
	#creatquest函数格式&createquest(http对象,管理后台地址,题目类型,$surveyid,序号,题目名,题目图片文件名,数组引用);
	#############################
	
	#申明并初始化部分数据
	#@options (选项名字,选项图片,..) 的规则排列
	my (@Options,$sort,$flagReference,@questid,$temp);
	$sort = 1;				#排序从1开始
	$flagReference = 0;		#默认都不是引用题
	@questid;		#引用题的引用源
	##type 题目类型:(1.单选  2.多选  3.尺度  4.数字题  5.文字填空  6.关联题  7.视频) 
	my %questitle = (
		'1' => '单选题',
		'2' => '多选题',
		'3' => '尺度题',
		'4' => '数字题',
		'5' => '文字填空题',
		'6' => '关联题',
		'7' => '视频题',
	);
	
	#初始化数据(单选) 1
	@Options = ("A1","","A2","","A3","","A4","","A5","");	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question(	$mgtbrowser,$mgt_addr,$questionType,$surveyid,$sort++,"$questitle{$questionType}","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(单选) 2
	@Options = ("A1","","A2","","A3","","A4","","A5","");	
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question(	$mgtbrowser,$mgt_addr,$questionType,$surveyid,$sort++,"$questitle{$questionType}","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(单选) 3
	@Options = ("A1","","A2","","A3","","A4","","A5","");
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question(	$mgtbrowser,$mgt_addr,$questionType,$surveyid,$sort++,"$questitle{$questionType}","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(单选) 4
	@Options = ("A1","","A2","","A3","","A4","","A5","");
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question(	$mgtbrowser,$mgt_addr,$questionType,$surveyid,$sort++,"$questitle{$questionType}","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;
	
	#初始化数据(单选) 5
	@Options = ("A1","","A2","","A3","","A4","","A5","");
	#执行创建题目函数(一次创建一个题目)
	$temp = &edit_question(	$mgtbrowser,$mgt_addr,$questionType,$surveyid,$sort++,"$questitle{$questionType}","",\@Options,$flagReference,\@questid,"");
	push @questid,$temp;

	return ($surveyid,\@questid,$mgtbrowser);
}


	
sub createSurvey_2
{
	#$logichash 不用转成数组直接进逻辑设置接口在转数组
	my ($mgt_addr,$mob_addr,$surveyid,$logichash,$mgtbrowser,$questid,$survey,$username) = @_;
	my @questid = @$questid;
	my @survey = @$survey;
	my @username = @$username;
	
	#创建逻辑
	&editlogic($mgtbrowser,$mgt_addr,$logichash);
	
	#创建配额
	#1女  0男
	my (@quota_1, @quota_2);
	#提交一个
	@quota_1 = (
		'age',"",
		'amount',"",
		'area',"",
		'reviewer','213',
		'reviewer','213',
		'sex','',
		'surveyCount',11,
		'surveyId',$surveyid,
		);
	@quota_2 = (
		'quotaType',"1",
		'totalSamples',"",
		'surveyId',$surveyid,
		);
	&addsurveyquota($mgtbrowser,$mgt_addr,$surveyid,\@quota_1,\@quota_2);

	
	
	####
	#提交问卷
	#survey/a/submitSurvey?surveyId=3150
	&submit($mgtbrowser,$mgt_addr,$surveyid);
	
	####
	#审核上家
	####
	my %auditconfig = (
		"awardAmount" => 2,
		"flagBooking" => "",
		"terminations[0].awardAmount" => "0.1",	#终止奖励金额
		"terminations[0].awardNum" => "50",		#终止奖励人数
		"terminations[0].questionId" => "$questid[0]",	#终止的题号
		"terminations[0].optionId" => "2",		#选项号
		"terminations[0].selected" => "1",		#1.选择
		"terminations[0].sendAwardNum" => "0",	
		"terminations[0].sortedNum" => "1",		#问题序号
	);
	&survey_pass($mgtbrowser,$mgt_addr,$surveyid,%auditconfig);
	
	
	
	####
	#立刻上架操作
	####
	my $start_time = &calculate_date(&nowtime("1","1","1","1","1","1",""),"minute","-","10");
	my $sql = "update SURVEY set START_TIME=\"$start_time\" where SURVEY_ID=$surveyid";
	&operate_mysql(@survey,"$sql","update");
	sleep 1;
	
	#手机端验证是否存在该问卷

	####
	#开始验证
	####
	if(&checkMySurvey($mob_addr,$surveyid,$username[0])){
		return $surveyid;
	}else{
		return 0;
	}


}
1;
#从页面列表中获取需要的信息
use strict;

sub survey_answer_list
{
	my ($mgtbrowser,$mgt_addr,$answer_list_url,$surveyid,$answerCode) = @_;
	
	#获取页码
	my $maxPage;
	my $myget = "$mgt_addr$answer_list_url/$surveyid/$answerCode";
	my $return_html = $mgtbrowser->get("$myget")->content;
	my $mypage = &xpath_find_value($return_html,'/html/body/div[2]/div/div[2]/div[3]/div/div[2]/div/div/div[3]/div[2]/div/ul/a');
	
	print "myget:$myget\n";
	print "mypage:$mypage\n";
	#截取页码
	if($mypage =~ /共(\d+)页/){
		$maxPage = $1;
	}
	print "maxpage:$1\n";
	next unless($maxPage);	#0就跳过k


}

1;
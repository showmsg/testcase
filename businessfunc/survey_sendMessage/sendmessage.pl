#发送消息
use strict;

sub sendmessage
{
	#发送消息
	#http://10.201.2.12/message/a/add_message  msgType=0全部用户 =2指定用户 =5用户组
	#$recipientid 用户组id
	my ($mgtbrowser,$mgt_addr,$ms_info,$msgtype,$recipientid,$ms_tital,$user_id) = @_;
	
	my $return_json = $mgtbrowser->post("$mgt_addr/message/a/add_message",
		{
		content=>$ms_info,
		id=>"",
		msgType=>$msgtype,
		recipientId=>$recipientid,
		recipientName=>"",
		subject=>$ms_tital,
		users=>$user_id,
	
		},
		'Referer'=>"$mgt_addr/message/message_edit",
		'Content-Type'=>"application/x-www-form-urlencoded; charset=UTF-8",
	)->content;
	#print "sendmassage - $return_json\n";
	&verification("mgt","sendmassage",$mgtbrowser,$return_json);
	
}
1;
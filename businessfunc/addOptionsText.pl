#单选和多选的生成题目按钮
use strict;

sub addOptionsText
{
	my ($mgtbrowser,$mgt_addr,$addOT_GET) = @_;
	#####
	#开始网络请求 点击生成
	#####
	my ($return_json,$questionId);
	
	$return_json = $mgtbrowser->get("$mgt_addr/survey/a/addOptionsText?$addOT_GET")->content;
	&verification("mgt","edit_question.addOptionsText",$mgtbrowser,$return_json);
	if($return_json =~ /"id":"(\d+)"/){
		$questionId = $1;
	}
	
	return $questionId;
}
1;
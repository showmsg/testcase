﻿#提交问卷http://10.201.2.13:8080/ec/addEBusiness

use strict;

sub addEBusiness
{
	my ($openbrowser,$open_addr,$parameter_group) = @_;
	
	my $return = $openbrowser->post("$open_addr/ec/addEBusiness",{%$parameter_group},
		'Content-Type'=>"multipart/form-data;",
	)->content;

	myprint("encode", "return:($return) \n");
	return &verification("mgt","addEBusiness",$openbrowser,$return);
}
1;
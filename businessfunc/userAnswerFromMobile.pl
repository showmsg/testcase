﻿#模拟用户开始答题

use strict;

sub userAnswerFromMobile
{
	my ($mob_addr,$surveyStr,$useraccount,$surveyid,$answerList) = @_;
	my @survey = @$surveyStr;
	my %answerList = %$answerList;
	
	#题目类型哈希
	my %questType = (
		"1" => "单选题",
		"2" => "多选题",
		"3" => "尺度题",
		"4" => "比一比",
		"5" => "踩or赞",
		"6" => "填空题-文字",
		"7" => "填空题-数字",
	);
	
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	#(获取选项id(sortNum), 获取题目类型, 获取选项总量, 获取是否终结的值)
	my ($answerSrt,$sortNum,$questionType,$optionCount,$isEnd); 
	
	#答题结果统计初始化
	my %danxCount = ();	#单选计算
	my %duoxCount = ();	#多选计算
	
	$isEnd = "false";
	#返回手机登录后的会话
	my ($mobrowser,$accessToken);
	
	#手机端的登录请求
	my @singleUser = ();	#保存单个用户的信息
	my $userString;			#组装用户信息后插入所有用户数组中（;号隔开元素  ,号隔开选项之间  :隔开题号与选项 -隔开多选题答案）
	chomp($useraccount);
	push @singleUser,$useraccount;	#第一个元素用户名
	&myprint("encode","$useraccount 用户登录..");

	#手机登录请求
	#'MTExMTExMTE=' => '11111111'
	#'MTIzNDU2' => 123456
	($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
	my $errCount = 0;
	while($mobrowser eq 0){	#登录失败尝试重新登录
		($mobrowser,$accessToken) = &moblogin($mob_addr,$useraccount,'MTExMTExMTE=');
		if($errCount++ eq 5){
			print "login err counts equal 5 and give up\n";
			return 0;
		}
		
	}

	#处理超时用户
	if($answerList{"timeout"}){		#超时
		&myprint("encode","超时用户..答题结束\n");
		push @singleUser,"timeout";
		return (join ";",@singleUser);
	}

	#初始化存放answserStep接口的报文参数
	my %answerStepGetInfo = ();
	$answerStepGetInfo{'surveyId'} = $surveyid;
	$answerStepGetInfo{'stepType'} = "2";
	
	#点击开始答题,系统会判断是否答过，没有答过就返回第一题的信息
	my $ReturnJson = &answerStepFromMobile($mobrowser,$accessToken,$surveyid,\%answerStepGetInfo);
	#处理返回的数据
	$json_obj = $json->decode("$ReturnJson");
	#获取选项id(sortNum)
	$sortNum = $json_obj->{'data'}->{'questionDetail'}->{'sortNum'};
	#获取题目类型
	$questionType = $json_obj->{'data'}->{'questionDetail'}->{'questionType'};
	#获取选项总量
	if($json_obj->{'data'}->{'questionDetail'}->{'optionScheme'}->{'options'}){
		$optionCount = @{$json_obj->{'data'}->{'questionDetail'}->{'optionScheme'}->{'options'}};
	}
	#获取是否答题结束的值
	$isEnd = $json_obj->{'data'}->{'isEnd'};
	
	
	&myprint("encode","开始答题..");
	
	#开始循环答题		
	while($isEnd eq "false"){

		#解析答案
		#从外面传进来的答案hash获得该题的答案
		#如果为空就说明需要脚本随机生成答案
		my $thisAnswer = "";
		$thisAnswer = $answerList{$sortNum};
		#print "*** thisAnswer - $thisAnswer ***";
		#$| =1 ;
		
		#解析预置答案,否则就随机回答问题
		#min,max存放范围值以便随机其中的值
		#@T_thisAnswerList,@thisAnswerList 前者是临时数组,后者存放可用的答案
		#@AnswerStr存放字符串答案，[0]为多选时的序号,[1]则是字符串
		my ($min,$max,@T_thisAnswerList,@thisAnswerList,@AnswerStr);
		if($thisAnswer){		
			@T_thisAnswerList = split /,/,$thisAnswer;
			for my $e (@T_thisAnswerList){
				if($e =~ /-/){	#这里组装范围值
					($min,$max) = split /-/,$e;
					for my $le ($min..$max){
						push @thisAnswerList,$le;
					}
					next;
				}
				if($e =~ /_/){	#这里如果有需要输入字符串的题目
					@AnswerStr = split /_/,$e;	#$AnswerStr[1]存放着文字答案
					next;
				}
				push @thisAnswerList,$e;	#这里输入普通的正常的答案
			}
		}else{
			#这里处理随机答案
			$thisAnswer = "random";
		}
		
		#测试
		#print "** $_ ***" for(@thisAnswerList);
		
		#清空answserStep接口的报文参数
		%answerStepGetInfo = ();
		my $optionid;
		#先组装一些不变的参数
		$answerStepGetInfo{'surveyId'} = $surveyid;
		$answerStepGetInfo{'stepType'} = "2";
		$answerStepGetInfo{"sortNum"} = $sortNum;

		
		#不同的类型组装答案 存入%answerStepGetInf中
		my $myanswer;
		&myprint("encode",$questType{$questionType});
		
		#单选题
		if($questionType eq "1"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = &random(1,$optionCount);
			}
			#统计
			#push @singleUser,("$sortNum-".$answerStepGetInfo{"answer"});
		}
		
		#多选
		if($questionType eq "2"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = &random(1,$optionCount);	#给予第一个选项值
				my $max = &random(1,$optionCount);
				for(2..$max){	#随机多选的选项个数，从第二个开始
					my $ch = &random(1,$optionCount);
					redo if($answerStepGetInfo{"answer"} =~ /$ch/);
					$answerStepGetInfo{"answer"} = $answerStepGetInfo{"answer"} . ",$ch";

				}
			}
			
			#统计
			#push @singleUser,("$sortNum-".$answerStepGetInfo{"answer"});
		}
		
		#尺度
		if($questionType eq "3"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = &random(1,5);
			}
		}
		
		#比一比
		if($questionType eq "4"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = &random(1,2);
			}
		}
		
		#赞or踩
		if($questionType eq "5"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = &random(1,2);
			}
		}
		
		#填空题-文字
		if($questionType eq "6"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = 'i love you';
			}
		}
		
		#填空题-数字
		if($questionType eq "7"){	
			if($thisAnswer eq 'random'){
				$answerStepGetInfo{"answer"} = '19850409';
			}
		}
		
		
		
		print "sortNum:$sortNum answer:". $answerStepGetInfo{"answer"} . " ";
		
		#报文组装完毕,请求答题接口
		$ReturnJson = &answerStepFromMobile($mobrowser,$accessToken,$surveyid,\%answerStepGetInfo);
		push (@singleUser,$answerSrt);
		while($ReturnJson eq 0){
			&myprint("encode","答题出现异常尝试重新请求\n");
			$ReturnJson = &answerStepFromMobile($mobrowser,$accessToken,$surveyid,\%answerStepGetInfo);
		}

		
		#处理返回的数据
		$json_obj = $json->decode("$ReturnJson");
		#获取选项id(sortNum)
		$sortNum = $json_obj->{'data'}->{'questionDetail'}->{'sortNum'};
		#获取题目类型
		$questionType = $json_obj->{'data'}->{'questionDetail'}->{'questionType'};
		#获取选项总量
		if($json_obj->{'data'}->{'questionDetail'}->{'optionScheme'}->{'options'}){
			$optionCount = @{$json_obj->{'data'}->{'questionDetail'}->{'optionScheme'}->{'options'}};
		}
		#获取是否答题结束的值
		$isEnd = $json_obj->{'data'}->{'isEnd'};

	}
	

	&myprint("encode","答题结束\n");
	
	return (@singleUser);


	
	sub answerStepFromMobile
	{
		#外部输入强制虚拟用户选择指定选项
		#$optionid强制选择的id, $unselectid强制不选择的id
		my ($mobrowser,$accessToken,$surveyid,$answerStepGetInfo) = @_;
		my %answerStepGetInfo = %$answerStepGetInfo;
		#题目类型哈希
		
		#开始答题
		#&myprint("encode","(sortNum:" . $answerStepGetInfo{'sortNum'} . "answer:". $answerStepGetInfo{'answer'} . ") ");
		#$|=1;	#清除缓存以便立即打印结果在终端
		
		#组装get参数
		my $getStr = "$mob_addr/survey-ws/vi/surveysvc/answerStep";
	
		#发送第一题的请求
		#&myprint("encode","answerStep ---  $getStr \n");
		my $returnJson = $mobrowser->post($getStr,{%answerStepGetInfo},
			'Authorization'=> "Bearer $accessToken",
			'Accept' => "application/json;charset=UTF-8",
		)->content;
		#&myprint("encode","debug-answerStep  ----  returnJson:$returnJson\n");
		
		my $ret = &verification("mob","survey_answer.answerStep",$mobrowser,$returnJson);
		if($ret eq 2 || $ret eq 4){
			return 0;	#返回后外面的$answerSrt接受异常值
		}
		
		return $returnJson;
	}
	
}
1;
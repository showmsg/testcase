#验证上架的问卷在手机端是否可以看到

use strict;

sub checkMySurvey
{
	my ($mob_addr,$surveyid,$username) = @_;
	#手机端的登录请求
	my ($mobrowser,$accessToken);
	($mobrowser,$accessToken) = moblogin($mob_addr,$username);
	print "username : $username\n";
	
	####
	#开始验证
	####
	my $return;
	my $c = 0;
	for(1..2){		#每秒请求一次,30秒后返回没有需要的值,测试失败	sortType=1奖励金额  surtType=2上架时间
		$return = $mobrowser->get("$mob_addr/survey-ws/surveysvc/homepageList?sortType=2&pagesize=10&type=1&page=0&surveyid=-1",
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",)->content;
		&verification("mob","survey_exist.surveys",$mobrowser,$return);
		#print "$return \n";
		while($return =~ /"surveyId":(.*?),/gi){
			#print "--debug-- $1\n";
			if($surveyid eq $1){
				print "find it -- surveyId:$1\n\n";
				$c++,last;
			}
		}
		last if($c);
		sleep 1;
	}
	unless($c){		#找不到surveyid就退出了
		print "Can not find surveyId -- $surveyid\n\n";
		return 0;
	}
	print LOG "exist - $return\n";
	return 1;
}
1;
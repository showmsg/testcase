﻿#模拟用户开始答题

use strict;

sub useraddress
{
	my ($mobrowser,$mob_addr,$accessToken,$addressPost) = @_;
	my %addressPost = %$addressPost;

	my $returnJson = $mobrowser->post("$mob_addr/survey-ws/useraddresssvc/useraddress",{%addressPost},
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	#&verification("mob","survey_answer.seckill",$mobrowser,$return);
	unless (&verification("mob","useraddress",$mobrowser,$returnJson)){
		return 0;
	}
	
	return $returnJson;
	
}
1;
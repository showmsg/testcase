﻿#点击抽奖 http://10.201.2.12:8007/executeLottery

use strict;

sub shareReport
{
	my ($mob_addr, $survey, $mobrowser, $accessToken, $reportId) = @_;
	
	my %login_head = (
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	);
	#print "==== $mob_addr, $accessToken, $reportId====\n";
	my $return = $mobrowser->post("$mob_addr/survey-ws/vi/reportsvc/shareReport",
		{
			reportId => $reportId,
		},
		%login_head,
	)->content;
	
	#print "==== $return ====\n";
	return 0 unless($return =~ /^{.*?}$/);

	return $return;
}
1;



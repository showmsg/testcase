﻿#模拟用户开始答题

use strict;

sub userCenter
{
	my ($mob_addr,$surveyStr,$mobrowser,$accessToken,$needWhat) = @_;	
	
	#处理json对象
	my $json = JSON->new->utf8;
	my $json_obj;
	
	#个人中心查询
	my $returnJson_2 = $mobrowser->get(
		"$mob_addr/survey-ws/usersvc/userCenter",
		'Authorization'=> "Bearer $accessToken",
		'Accept' => "application/json;charset=UTF-8",
	)->content;
	unless (&verification("mob","doErnie",$mobrowser,$returnJson_2)){
		return 0;
	}
	$json_obj = $json->decode($returnJson_2);
	
	#根据$needWhat的值返回适当的数据
	unless($needWhat){	#如果needWhat为空把整个hash返回回去
		return \%{$json_obj->{'data'}};
		
	}
	for(keys %{$json_obj->{'data'}}){	#如果找到需要的key就返回对应的value
		return $json_obj->{'data'}->{$_} if($needWhat eq $_)
	}
	
	#否则就返回一个0
	return 0;
}
1;

__END__
userCenter可查找到的key 与其可返回的value
"data": {
        "loginName": "15821917172",   // 赚零用账号
        "nickName": "Fanny",          // 昵称
        "avatar":用户图像地址
        "signature":个性签名
        "sex": 1,         f            //性别 -1：人妖 0：男 1：女
        "currentAmount": 2.1,          //账户余额
        "currScore":100,              // 当前剩余积分
        "totalScore":1000,            // 获得总积分
        "isCellphoneAuth": true,        //手机是否验证
        "userDetailFinished": false,      //个人信息是否完全
        "hasAddress": false,           //是否有收货地址
        "isOpenBank":false,
        "isPerfectBank":false,
        "isDailySign":true,             //是否已签到
        "uploadToken":七牛云上传token,
        "inviteCode": "zygwyzkw",    //邀请码
        "scoreEnterLink":积分奖励分配的连接入口,
        "notErnieCount":未摇奖的数量
    }

#这里就是一些配置

use strict;

sub config
{
	my ($where,$what) = @_;
	my (@survey, $mgt_addr, $mob_addr, $open_addr, $wap_addr, $ad_addr);
	
	#如果没有设置那就默认为... 这里可以统一配置
	$where = "0305" unless($where);
	
	if($where eq "1205"){
		#数据库链接信息
		@survey = ("survey_1205","10.201.2.13","3306","mq-user","redhat");
		#管理后台地址
		$mgt_addr = "http://10.201.2.12";
		#手机服务地址
		$mob_addr = "http://10.201.2.12:8080";
		#开放平台地址
		$open_addr = "http://10.201.2.13:8080";
		#抽奖地址
		$wap_addr = "http://10.201.2.12:8007";
		#广告系统地址
		$ad_addr = "http://10.201.2.12:8000";
	}
	if($where eq "0305"){
		#数据库链接信息
		@survey = ("survey_0305","10.201.2.13","3306","mq-user","redhat");
		#管理后台地址
		$mgt_addr = "http://10.201.70.81";
		#手机服务地址
		$mob_addr = "http://10.201.70.82:8080";
		#开放平台地址
		$open_addr = "http://10.201.70.81:8080";
		#抽奖地址
		$wap_addr = "http://10.201.70.81:8007";
		#广告系统地址
		$ad_addr = "http://10.201.70.81:8000";
	}
	
	return @survey if($what eq "sql");
	return $mgt_addr if($what eq "mgt_addr");
	return $mob_addr if($what eq "mob_addr");
	return $open_addr if($what eq "open_addr");
	return $wap_addr if($what eq "wap_addr");
	return $ad_addr if($what eq "ad_addr");
	
}
1;

#打印函数
use strict;

sub myprint
{
	my ($command,$par1,$par2,$par3) = @_;
	
	if($command eq "encode"){
		$par1 = encode("gbk", decode("utf-8", $par1));
		print $par1;
		return $par1;
	}

	if($command eq "start"){
		print "#" x 10 ."\n";
		print "start\n";
		print "#" x 10 ."\n";
		return 1;
	}
	
	if($command eq "end"){
		print "#" x 10 ."\n";
		print "end\n";
		print "#" x 10 ."\n";
		return 1;
	}
	
	if($command eq "description"){
	#将打印描述的功能移到开始方法中执行,次方法不做实现
	#print "command = description\n";
	#&myprint("description",1) 说明第二个参数读取用例的说明编号
	#	open FF,"funclist1.txt";
	#	my @data1 = <FF>;
	#	close FF;
	#	for(@data1){
	#		my @d = split(/\s+/,$_);
	#		if($d[0] eq $par1){
	#			print "$d[1] $d[2]\n";
	#		}
	#	}
		return 1;
	}
	
	#返回ReturnStart.pl需要的可执行的脚本列表
	#&myprint函数的控制命令和文件名&myprint("readFuncList","文件名")
	if($command eq "readFuncList"){
		open FF,"$par1";
		my @data;
		for(<FF>){
			push @data,$_ unless(/^#|^\s+$/);
		}
		close FF;
		return @data;
	}

	
	#command值没有正确的处理
	print "function myprint ,command value undefind!!\n";
	
}
1;

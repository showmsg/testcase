#读取Data目录下的一个文件，然后返回读取的数据
#%data = &readExcel("1.xls","Sheet1"); 	for my $index (1..$data{'maxDataLine'}){}
#print $data{'age'} -> [1] . "\n";


sub readExcel
{	
	#获取文件名，工作表名
	my ($filename,$SheetName) = @_;
	my %Datahash;
	
	my $parser   = Spreadsheet::ParseExcel->new();
	my $workbook = $parser->parse("data\\$filename");
	
	if ( !defined $workbook ) {
		die $parser->error(), ".\n";
	}
	
	#根据参数读取excel中的sheet工作表
	for my $worksheet ( $workbook->worksheets() ) {
		my $worksheetString = $worksheet->get_name();
		$worksheetString = encode("gbk", $worksheetString);
		next unless($worksheetString eq $SheetName);
		
		my ( $row_min, $row_max ) = $worksheet->row_range();	#row 行
		my ( $col_min, $col_max ) = $worksheet->col_range();	#col 列
	
		#获取数据文件的最大数据行不算field行
		my $maxDataLine = int($row_max) - 1;
		$Datahash{'maxDataLine'} = $maxDataLine;
		
		for my $col ( $col_min .. $col_max ){	#列
			#将字段名输入返回的哈希Datahash中
			my $colCell = $worksheet->get_cell( 0, $col );
			next unless $colCell;
			my $f = $colCell->value();
			
			#去除括号的注释内容
			#print "--debug \-- $f \n";
			if($f =~ /^(.*?)\(.*?\)(.*?)$/){
				#print "---- $1 -- $2 ---\n";
				$f = $1 . $2;
			}
			#跳过注释字段
			next if($f =~ /^#/);
			#清空这个数组地址中的残留数据
			@{'saveValue'.$f} = ();
			#将应用装入hash中
			$Datahash{$f} = \@{'saveValue'.$f} ;
			#将值存入数组再装入hash
			for my $row ( 1 .. $row_max ){	#行
				my $rowCell = $worksheet->get_cell( $row, $col );
				#空处理
				unless($rowCell){
					push $Datahash{$f}, "";
					next;
				}
				my $rv = $rowCell->value();
				$rv = encode("utf-8",$rv);
				push $Datahash{$f}, $rv;
			}
		}
	}

	#测试代码
#	while(my($k,$v) = each %Datahash){
#		print "k:$k -- v:$v";
#		for(@$v){
#			myprint("encode", " ($_) ");
#		}
#		print "\n";
#	}
#	print "------\n";
	
	#拿去吧！你要的数据...不要谢我...
	return %Datahash;
}
1;
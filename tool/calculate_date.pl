#时间计算函数

#单元测试
#print "2000-03-10 09:58:50\n";
#open FF,">log.txt";
#for(1..1000000){
#	print  &calculate_date("2000-03-10 09:58:50","second","+","$_")."\n";
#	<>;
#}

sub calculate_date
{
	#传入yyyy-mm-dd hh:mm
	#year年 month月 day日 hour小时 minute分钟 second秒
	#+时间 -时间
	my ($incomedate, $timeType, $ctrl, $much) = @_;
	my @date = split /\s+|-|:/,$incomedate;
	my ($year,$month,$day,$hour,$minute,$second) = @date;
	
	my ($totalsecond,$oldsecond);  
	$much = $much if ($timeType eq "second");	#默认处理秒
	$much = $much * 3600 if ($timeType eq "hour");	#处理小时;
	$much = $much * 60 if ($timeType eq "minute");	#处理分钟;
	#if ($timeType eq "day")		#处理天
	#if ($timeType eq "month")	#处理月
	#if ($timeType eq "year")	#处理年
	
	$oldsecond = $hour*3600 + $minute*60 + $second;
	$totalsecond = $oldsecond + $much if($ctrl eq "+");
	$totalsecond = $oldsecond - $much if($ctrl eq "-");
	$hour = $totalsecond / 3600;
	$minute = ($totalsecond % 3600) / 60;
	$second = ($totalsecond % 3600) % 60;
	
	$hour = 0 if($hour<0 || $hour>24);	#只处理一天的时间增减,临时解决方法
	
	for($hour,$minute,$second){
		$_ = sprintf "%02d",$_;
	}
	#return "$date[0]-$date[1]-$date[2] $hour:$minute:$second";
	return "$date[0]-$date[1]-$date[2] $hour:$minute";
}
1;
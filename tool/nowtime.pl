#返回当前时间函数
sub nowtime
{	
	#格式
	#&nowtime("yyyy","mm","dd","hh","mm","ss","www")
	
	#<返回具体时间>
	#传入1返回需要的具体时间 小时:分钟:秒 hh:mm:ss
	#不传就不返回
	#&nowtime("","","","1","1","1","")
	
	#<验证具体时间>
	#传入具体时间,验证是否是到达当前时间
	#如果是5点1分要传入05 01 , 否则会当作<返回具体时间>处理
	#返回1是当前时间，返回0不是当前时间
	#&nowtime("","","","14","15","44","")
	
	my @need_return = @_;
	my %month = (
		"Jan" => "01","Feb" => "02","Mar" => "03","Apr" => "04",
		"May" => "05","Jun" => "06","Jul" => "07","Aug" => "08",
		"Sep" => "09","Oct" => "10","Nov" => "11","Dec" => "12",
	);
	
	my $local_time = localtime();
	my @date = split /\s+/,$local_time;
	$date[2] = sprintf "%02d",$date[2];
	my @time = split /:/,$date[3];
	my @now_time = ($date[4],$month{$date[1]},$date[2],$time[0],$time[1],$time[2],$date[0]);
	
	#全是空值直接返回0
	my $i = 0;
	for(@need_return){
		if( $_ eq "" ){
			$i++;
			return 0 if( $i == 7 );
			next;	
		}
		if( $_ == 1 ){
			#有一个参数是 1 说明要返回具体时间
			my $return_time;
			$return_time = $return_time . $date[4] if ( $need_return[0] ne "" );			#年
			$return_time = $return_time . "-" if ( $need_return[0] ne "" && $need_return[1] ne "");	#-
			$return_time = $return_time . $month{$date[1]} if ( $need_return[1] ne "" );	#月
			$return_time = $return_time . "-" if ( $need_return[1] ne "" && $need_return[2] ne "");	#-
			$return_time = $return_time . $date[2] if ( $need_return[2] ne "" );			#日
			$return_time = $return_time . " " if ( $need_return[2] ne "" && $need_return[3] ne "");	#空格
			$return_time = $return_time . $time[0] if ( $need_return[3] ne "" );			#时
			$return_time = $return_time . ":" if ( $need_return[3] ne "" && $need_return[4] ne "");	#:
			$return_time = $return_time . $time[1] if ( $need_return[4] ne "" );			#分
			$return_time = $return_time . ":" if ( $need_return[4] ne "" && $need_return[5] ne "");	#:
			$return_time = $return_time . $time[2] if ( $need_return[5] ne "" );			#秒
			$return_time = $return_time . " " if ( $need_return[5] ne "" && $need_return[6] ne "");	#空格
			$return_time = $return_time . $date[0] if ( $need_return[6] ne "" );			#周
			return $return_time;
		}else{
			#否则比对时间
			my $n = 0;
			for(@need_return){
				if( $_ ne "" ){
					return 0 if( $_ ne $now_time[$n]);
				}
				$n++;
			}
			return 1;
		}
	}
}
1;
#连接数据库

use strict;
	
#连库函数
sub operate_mysql
{
	#&operate_mysql(@db_name,"$sql_word","select|insert|update");
	my @parameter = @_;
	my $control = $parameter[6];
	my $dbname = $parameter[0];
	my $location = $parameter[1];
	my $port = $parameter[2];
	my $database = "DBI:mysql:$dbname:$location:$port";
	my $db_user = $parameter[3];
	my $db_pass = $parameter[4];
	my $dbh = DBI->connect($database,$db_user,$db_pass);
	my $sql_word = $parameter[5];

	#print "debug--sqlword--: $sql_word\n";
	
	if ( $control eq "select" ){
		my $sth = $dbh -> prepare($sql_word);
		$sth -> execute() or print $dbh->errstr;
		my @result ;
		while (my @value = $sth -> fetchrow_array){
			push @result, @value;
			#print "value : @value\n";
		}
		return $result[0] if ($#result == 0);
		return @result;
	}
	if ( $control eq "insert" ){
		my $sth = $dbh -> prepare($sql_word);
		$sth -> execute() or print $dbh->errstr;
	}
	if ( $control eq "update" ){
		my $sth = $dbh -> prepare($sql_word);
		$sth -> execute() or print $dbh->errstr;
	}
}

#同样是数据库操作，相对前一个函数更加效率
sub connection
{	
	#@uhomedb_test = ("uhomedb_test","116.228.70.232","3306","uhomer","uhome");
	my ($dbname,$location,$port,$db_user,$db_pass);
	my ($database,$dbh);
	($dbname,$location,$port,$db_user,$db_pass) = @_;
	$database = "DBI:mysql:$dbname:$location:$port";
	return $dbh = DBI->connect($database,$db_user,$db_pass);
}
sub operate_mysql_new
{
	#&operate_mysql($dbh,"$sql_word","select|insert");
	my ($dbh,$sql_word,$control);
	($dbh,$sql_word,$control) = @_;


	if ( $control eq "select" ){
		my $sth = $dbh -> prepare($sql_word);
		$sth -> execute() or print $dbh->errstr;
		my @result ;
		while (my @value = $sth -> fetchrow_array){
			push @result, @value;
			#print "value : @value\n";
		}
		return $result[0] if ($#result == 0);
		return @result;
	}
	if ( $control eq "insert" ){
		my $sth = $dbh -> prepare($sql_word);
		$sth -> execute() or print $dbh->errstr;
	}
}

1;